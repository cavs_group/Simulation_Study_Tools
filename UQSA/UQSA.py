__version__ = "1.4.8"
__date__ = "03/21/2017"
'''

Copyright (c) 2017 Justin M. Hughes

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''
try:
    import Tkinter as tk
    from Tkinter import Frame, Tk, BOTH, Text, Menu, END
except ImportError:
    from tkinter import FRAME, Tk, BOTH, Text, Menu, END
    import tkinter as tk
#Import iterable model version
try:
    from src.user_model_wrapper import wrapper
    #print "\n\n\n\n'user_model_wrapper.py' module loaded..."
except:
    #print "\n\n\n\n'user_model_wrapper.py' not found. Exiting..."
    quit()

try:
    from src import RBF
    #print "'RBF.py' module loaded..."
except:
    #print "'RBF.py' not found. Exiting..."
    quit()

#Import single model point version
#from src.user_model_wrapper import wrapper_EFAST

import tkFileDialog
import os,sys,shutil,tkFont
import gc

current_directory = os.getcwd()
os.chdir('src')
source_directory = os.getcwd()
os.chdir(current_directory)

try:
    import numpy as np
    #print "'NumPy' module loaded..."
except:
    #print "'NumPy' not installed. Exiting..."
    quit()

gc.enable()

class MCGUI(Frame):
    startdir = str(current_directory)
    paramfile = "param_default.csv"
    modelfile = "model_default.txt"
    expfile = "experiment.csv"
    paramlabels = []
    wrkdir = str(current_directory)
    srcdir = str(source_directory)
    os.chdir(wrkdir)
    mcsfields = ('Min X','Max X','X Variable','Model Points','Simulations')
    mcsdefaults = ("0","10",'x',"10","1000")
    efastfields = ('Evals Per Step', 'EFAST Resamples')
    efastdefaults = ("1000","5")
    err = []
    rbfLda = "rbf_exp_default.csv"
    ldaMatrix = []
    setSurrogate = False
    rbfTrainDir = "rbf_train_default.csv"
    rbfTrain = []
    rbfData = []
    rbfTrainSet = False
    rbfDataSet = False
    rbfC = 1.0
    rbfType = 3
    steps_from_rbf_train = 10

    def getDir(self):
        try:
            self.wrkdir = tkFileDialog.askdirectory(**self.dir_opt)
            print "\nWorking directory set as: %s" % self.wrkdir
        except:
            print "\nNo direcotry selected..."

    def rbfSave(self,c1,rbf_selection):
        setSurrogate = True
        self.rbfC = float(str(c1.get()))

        if rbf_selection.get() == 'Thin Plate':
            func = "Thin Plate"
            self.rbfType = 1
        elif rbf_selection.get() == 'Gaussian':
            func = "Gaussian"
            self.rbfType = 2
        elif rbf_selection.get() == 'Multiquadric':
            func = "Multiquadric"
            self.rbfType = 3
        elif rbf_selection.get() == 'Inverse Multiquadric':
            func = "Inverse Multiquadric"
            self.rbfType = 4
        else:
            func = "Multiquadric"
            self.rbfType = 3
        
        print "RBF settings saved..."
        print "Using %s RBF type" %func
        print "UQSA model wrapper set for surrogate mode..."
        
    def rbfWindow(self):
        self.top = tk.Toplevel()
        self.top.title("RBF Settings")
        self.top.geometry("300x150+30+30")
        self.top.transient(self)
        constFrame = tk.Frame(self.top)
        typeFrame = tk.Frame(self.top)
        buttonFrame = tk.Frame(self.top)

        l1 = tk.Label(constFrame,text="RBF Constant (c)")
        c1 = tk.Entry(constFrame)
        c1.insert(0,str(self.rbfC))
        l1.pack(side='left',padx=5,pady=5)
        c1.pack(side='left',padx=5,pady=5)

        typeVar = tk.StringVar()
        typeVar.set('Multiquadric')
        l2 = tk.Label(typeFrame,text="RBF Type (1-4)")
        rbf_selection = tk.OptionMenu(typeFrame,typeVar,'Thin Plate','Gaussian', 'Multiquadric', 'Inverse Multiquadric')
        
        l2.pack(side='left',padx=5,pady=5)
        rbf_selection.pack(side='left',padx=5,pady=5)

        b1 = tk.Button(buttonFrame,text='Save Settings',font=self.customFont, command=lambda: self.rbfSave(c1,typeVar))
        b1.pack(side='bottom',padx=5,pady=5)

        constFrame.pack(side='top',padx=5,pady=5)
        typeFrame.pack(side='top',padx=5,pady=5)
        buttonFrame.pack(side='top',padx=5,pady=5)

        
    def __init__(self, parent):
        print "\n\n\n\n\n"
        print "\t--------------------------------------------------------------------\n"
        print "\t  Uncertainty Quantification and Sensitivity Analysis (UQSA) Tool\n"
        print "\t\t\t\tVersion: %s\n" % __version__
        print "\t\t\t\tDate: %s\n" %__date__
        print "\t    recent changes: - Added total effect index calculations"
        print "\t                    - Added option for multiple curve samples\n"
        print "\t--------------------------------------------------------------------\n"
        print "\tForward Uncertainty Quantification (UQ) and Sensitivity Analysis (SA)."
        print "\t\t\tauthor: Justin Hughes, justin.m.hughes@outlook.com"
        print "\t\t\t\tMore Info: icme.hpc.msstate.edu\n\n\n"

        Frame.__init__(self, parent)
        self.customFont = tkFont.Font(family='Helvetica',size=18)
        self.parent = parent
        self.initUI()
        self.dir_opt = options = {}

        self.param_file_opt = options = {}
        options['defaultextension'] = '.*'
        options['filetypes'] = [('CSV Files', '*.csv'), ('Text files', '.txt'), ('All files', '*.*')]
        options['initialdir'] = self.wrkdir
        options['initialfile'] = ''
        options['parent'] = self
        options['title'] = 'Select File'
        
        self.mod_file_opt = options = {}
        options['defaultextension'] = '.*'
        options['filetypes'] = [('Model Files', '*.mdl'), ('Text files', '.txt'), ('All files', '*.*')]
        options['initialdir'] = self.wrkdir
        options['initialfile'] = ''
        options['parent'] = self
        options['title'] = 'Select File'
        
        self.dir_opt = options = {}
        options['initialdir'] = self.wrkdir
        options['mustexist'] = False
        options['parent'] = self
        options['title'] = 'Select Working Directory'

    def gendefaults(self,wrkdir):
        os.chdir(self.wrkdir)
        if os.path.isfile("params_default.csv") == False:
            default_params = open("params_default.csv",'w')
            default_params.write("#Parameter,Value,STDEV,DIST TYPE,0=NORMAL,1=UNIFORM\n"
                             "A,1,0.05,0\nB,0.5,0.005,0\nC,2,0.05,0")
            default_params.close()
        else:
            print "Default parameter file already present."

        if os.path.isfile("model_default.mdl") == False:
            default_model = open("model_default.mdl",'w')
            default_model.write(
                "#PLACE USER DEFINED FUNCTIONS TO BE CALLED HERE\n"
                "#__startsub__ - DO NOT DELETE THIS LINE\n"
                "def subname(indata):\n"
                "\t## USER MATH STUFF HERE ##\n"
                "\toutdata = indata**2.0\n"
                "\treturn outdata\n"
                "def rejection_method(a):\n"
                "\t# User defined sampling rejection method.\n"
                "\t# DO NOT CHANGE THE FUNCTION NAME\n"
                "\treject = False\n"
                "\treturn reject\n"
                "#__endsub__ - DO NOT DELETE THIS LINE\n\n"
                "#Write model output to main model variable for MC analysis\n"
                "#Only data written to 'output' will be analyzed.\n"
                "#__startmodel__ - DO NOT DELETE THIS LINE\n"
                "reject = reject\n"
                "output = (A*exp(B*x)*x)**C")
            default_model.close()
        self.genRbf()
    
    def genRbf(self):
        os.chdir(self.wrkdir)
        if os.path.isfile("RBF.mdl"):
            os.remove("RBF.mdl")
        
        # Getting around a random bug for overwriting a file...
        if os.path.isfile("RBF.mdl") == False:
            rbf_model = open('RBF.mdl','w')
            rbf_model.write(
                "#PLACE USER DEFINED FUNCTIONS TO BE CALLED HERE\n"
                "#__startsub__ - DO NOT DELETE THIS LINE\n"
                "def get_norm(vec):\n"
                "\t# Returns the 2-norm (Euclidean distance) of vec\n"
                "\ta = 0\n"
                "\tfor i in range(0,len(vec)):\n"
                "\t\ta += vec[i]**2.0\n"
                "\tif a == 0:\n"
                "\t\treturn 0\n"
                "\telse:\n"
                "\t\treturn np.sqrt(a)\n\n"
                "def get_phi(r,c,rbf_type):\n"
                "\t'''\n"
                "\tRadial Basis Function (RBF) script for training RBF model\n"
                "\t\tHandles a single test point (for MC sampling)\n"
                "\t\tFor MC sampling, pass lda matrix to mc_rbf\n"
                "\n"
                "\tx - training points: [num_points by num_vars] (numpy.array)\n"
                "\ty - vector of function values for each x [by row] (numpy.array)\n"
                "\tx_test - test point for predicting with RBF (numpy.array)\n"
                "\tc - RBF constant\n"
                "\trbf_type - radial basis function to be used\n"
                "\t\t1 - Thin plate\n"
                "\t\t2 - Gaussian\n"
                "\t\t3 - Multiquadric\n"
                "\t\t4 - Inverse Multiquadric\n"
                "\t'''\n"
                "\tif rbf_type == 1:\n"
                "\t\tif r==0:\n"
                "\t\t\treturn 0\n"
                "\t\telse:\n"
                "\t\t\treturn np.log(c*r)*r*r\n"
                "\telif rbf_type == 2:\n"
                "\t\treturn np.exp(-1.0*c*r*r)\n"
                "\telif rbf_type == 3 or rbf_type == 4:\n"
                "\t\tphi = np.sqrt(r*r + c*c)\n"
                "\t\tif rbf_type == 4:\n"
                "\t\t\tphi = 1.0/phi\n"
                "\t\treturn phi\n"
                "\n"
                "def norm_params(vec,test='none'):\n"
                "\tnormvec = vec-vec\n"
                "\tif type(test) != type(vec):\n"
                "\t\tfor j in range(0,normvec.shape[1]):\n"
                "\t\t\tmaxval = max(vec[:,j])\n"
                "\t\t\tminval = min(vec[:,j])\n"
                "\t\t\tnormvec[:,j] = (vec[:,j]-minval)/(maxval-minval)\n"
                "\t\treturn normvec\n"
                "\telif type(test) == type(vec):\n"
                "\t\tnormtest = test-test\n"
                "\t\tfor j in range(0,normvec.shape[1]):\n"
                "\t\t\tmaxval = max(vec[:,j])\n"
                "\t\t\tminval = min(vec[:,j])\n"
                "\t\t\tnormvec[:,j] = (vec[:,j]-minval)/(maxval-minval)\n"
                "\t\t\tnormtest[:,j] = (test[:,j]-minval)/(maxval-minval)\n"
                "\t\treturn normvec,normtest\n"
                "\n"
                "def ypred_rbf(lda,x_train,x_test,c,rbf_type):\n"
                "\n"
                "\t#Normalize each dimension by max expected value\n"
                "\tx_train,x_test = norm_params(x_train,x_test)\n"
                "\n\n"
                "\ty_pred = np.zeros((lda.shape[1],1))\n"
                "\t#Get RBF prediction for test point at each x\n"
                "\tfor j in range(0,len(lda[0,:])):\n"
                "\t\t#print len(y[0,:])\n"
                "\t\tfor i in range(0,len(lda[:,0])):\n"
                "\t\t\tr = get_norm(x_test[0,:] - x_train[i,:])\n"
                "\t\t\tphi = get_phi(r,c,rbf_type)\n"
                "\t\t\ty_pred[j,0] += lda[i,j]*phi\n"
                "\t\t\t#print r, phi\n"
                "\treturn y_pred\n"
                "\n"
                "def rejection_method(a):\n"
                "\t# User defined sampling rejection method.\n"
                "\t# DO NOT CHANGE THE FUNCTION NAME\n"
                "\treject = False\n"
                "\treturn reject\n"
                "#__endsub__ - DO NOT DELETE THIS LINE\n"
                "\n"
                "#Write model output to main model variable for MC analysis\n"
                "#Only data written to 'output' will be analyzed.\n"
                "#__startmodel__ - DO NOT DELETE THIS LINE\n"
                "#__SURROGATE__ - DO NOT DELETE THIS LINE (Enables surrogate model wrapping)\n"
                "\n"
                "# SURROGATE MODEL SETTINGS\n"
                "# Set default rejection setting\n"
                "reject = False\n"
                "#Get lambda matrix\n"
                "Nsteps = lda.shape[1]\n"
                "dxx = float((xxmax-xxmin)/(Nsteps-1))\n"
                "xx = zeros([Nsteps,1],dtype=float)\n"
                "rr = [False]*len(xx)\n"
                "props = np.array(props).reshape(1,len(props))\n"
                "\n"
                "xx = [xxmin+i*dxx for i in range(0,Nsteps)]\n"
                "\n"
                "'''\n"
                "########################>  RBF SETTINGS <########################\n"
                "'''\n"
                "c = %s\n"
                "rbf_type = %s\n"
                "'''\n"
                "#################################################################\n"
                "'''\n"
                "\n"
                "reject = reject\n"
                "output = ypred_rbf(lda,xtrain,props,c,rbf_type)\n" %(self.rbfC,self.rbfType))
            rbf_model.close()
            print "\nRBF model file written to: %s" %self.wrkdir
            if os.path.isfile('RBF.mdl'):
                self.modelfile = os.path.abspath('RBF.mdl')
                print "\nModel file set as: %s" %self.modelfile

    def makeform(self,frame,fields,fielddefaults):
        entries = []
        count = 0
        if str(type(fields)) == "<type 'tuple'>":
            for field in fields:
                row = tk.Frame(frame)
                lab = tk.Label(row, width=15, text=field, anchor='w')
                ent = tk.Entry(row)
                row.pack(side='top',fill='x',padx=5,pady=5)
                lab.pack(side='left')
                ent.pack(side='right',expand='yes',fill='x')
                ent.insert(0,fielddefaults[count])
                entries.append((field,ent))
                count+=1
        else:
            row=tk.Frame(frame)
            lab = tk.Label(row,width=15,text=fields,anchor='w')
            ent = tk.Entry(row)
            row.pack(side='top',fill='x',padx=5,pady=5)
            lab.pack(side='left')
            ent.pack(side='right',expand='yes',fill='x')
            ent.insert(0,fielddefaults)
            entries.append((fields,ent))
            
        return entries
            

    def startmc(self,wrkdir,paramfile,modelfile,mcvar,savar,povar,fastvar,setupinfo,fastinfo,plotinfo,plotswitches):
        if os.path.isfile('mcsetup.dat'):
            os.remove('mcsetup.dat')
   
        os.chdir(self.srcdir)
        suf = open('mcsetup.dat','w')
        suf.write(wrkdir+'\n')
        suf.write(paramfile+'\n')
        suf.write(modelfile+'\n')
        suf.write(str(mcvar[0].get())+'\n')
        suf.write(str(mcvar[1].get())+'\n')
        suf.write(str(savar[0].get())+'\n') 
        suf.write(str(savar[1].get())+'\n')
        suf.write(str(savar[2].get())+'\n')
        suf.write(str(savar[3].get())+'\n')
        suf.write(str(savar[4].get())+'\n')
        suf.write(str(savar[5].get())+'\n')
        suf.write(str(povar[0].get())+'\n')    
        suf.write(str(povar[1].get())+'\n')   
        suf.write(str(povar[2].get())+'\n')

        count = 0   
        for e in setupinfo:
            if count == 3:
                if self.setSurrogate == False:
                    suf.write(str(int(e[1].get())-1)+'\n')
                else:
                    suf.write(str(self.steps_from_rbf_train-1))
            else:
                suf.write(str(e[1].get())+'\n')
            count += 1
        
        indvar = setupinfo[2][1].get()
        suf.write(indvar+'\n')
        
        for f in fastinfo:
            suf.write(str(f[1].get())+'\n')
        suf.write(str(fastvar[0].get())+'\n')
        suf.write(str(fastvar[1].get())+'\n')

        for p in plotinfo:
            suf.write(str(p[1].get())+'\n')
       
        for p in plotswitches:
            suf.write(str(p.get())+'\n')
        suf.close()        
        if os.path.isfile('user_model.pyc'):
            os.remove('user_model.pyc')
        if os.path.isfile('user_model.py'):
            os.remove('user_model.py')
        #print indvar
        '''   Settings order for file output                                                                  
        Variable Order in MCsetup.dat
        0     Work Directory
        1     Parameter File
        2     Model File
        3     Propagation Method
        4     Monte Carlo
        5     Derivatives
        6     Total Effect Index
        7     Vin/Vout Ratio
        8     Sobol Index
        9     FAST
        10    Scatter Plots
        11    Swap X/Y
        12    Log(X) axis
        13    Log(Y) axis
        14    Min X
        15    Max X
        16    Independent Variable Label
        17    Model Evaluation Points
        18    Number simulations
        19    Independent Variable
        20    FAST model points
        21    EFAST Resamples
        22    Frequency Type
        23    Shuffle Frequencies
        24    X axis label
        25    Y axis label
        26    Swap X/Y
        27    Log X
        28    Log Y
        '''
        os.chdir(self.wrkdir)


        umwerr,labels_from_params = wrapper(modelfile,paramfile,self.srcdir,indvar)
        # Uncomment to debug GUI
        #return 0

        # Surrogate model debugging
        #if self.setSurrogate:
        #    return 0
        
        self.err.append(umwerr)
        # Start main analysis script
        from src.main import mca_main
        self.err.append(mca_main(labels_from_params,self.srcdir,self.ldaMatrix,self.rbfTrain))

        # clean up temporary files
        #os.chdir(self.srcdir)
        #if os.path.isfile('mcsetup.dat'):
        #    os.remove('mcsetup.dat')
        #if os.path.isfile('user_model.pyc'):
        #    os.remove('user_model.pyc')
        #if os.path.isfile('user_model.py'):
        #    self.copyfile(os.path.realpath('user_model.py'),self.wrkdir)

##############################################################################

###########################   GUI SETUP   ####################################

##############################################################################
    def initUI(self):
##############################################################################
        # Initialize widget frames
        self.parent.title("UQSA Tool v%s" %__version__)
        baseFrame = tk.Frame(self)
        mcsFrame = tk.LabelFrame(baseFrame,text='Simulation Setup')
        FASTFrame = tk.LabelFrame(baseFrame,text='FAST Settings')
        FASToptionframe = tk.Frame(FASTFrame)
        figureFrame = tk.Frame(self)
        buttonFrame = tk.Frame(self)
        plotBaseFrame = tk.LabelFrame(baseFrame,text="Plot Settings")
        plotLeftFrame = tk.Frame(plotBaseFrame)
        plotRightFrame = tk.Frame(plotBaseFrame)
        self.pack(fill=BOTH, expand=1)

##############################################################################
        # Set up option storage variables
        mcvar = (tk.BooleanVar(),tk.BooleanVar())
        savar = (tk.BooleanVar(),tk.BooleanVar(),tk.BooleanVar(),tk.BooleanVar(),tk.BooleanVar(),tk.BooleanVar())
        povar = (tk.BooleanVar(), tk.BooleanVar(), tk.BooleanVar())
        fastvar = (tk.StringVar(),tk.BooleanVar())
        plotvar = (tk.BooleanVar(),tk.BooleanVar(),tk.BooleanVar())
        fastvar[0].set("Primes")

##############################################################################
        ## Set up drop down menus ##
        # Set up work directory, file options menu
        menubar = Menu(self)
        self.parent.config(menu=menubar)

        fileMenu = Menu(menubar)
        fileMenu.add_command(label="1. Set Working Directory", \
                             command=lambda: self.getDir())
        fileMenu.add_command(label="2. Select Model File", \
                             command=lambda: self.getMod())
        fileMenu.add_command(label="3. Select Parameter File", \
                             command=lambda: self.getParam())
        menubar.add_cascade(label="File Options", menu=fileMenu)

##############################################################################
        # Set up RBF menu
        rbfMenu = Menu(self)
        rbfMenu.add_command(label="1. RBF Settings", command = lambda: self.rbfWindow())
        rbfMenu.add_command(label="2. Select Training Points for RBF Surrogate", \
                             command=lambda: self.getRbfParams())
        rbfMenu.add_command(label="3. Select Training Data for RBF Surrogate", \
                             command=lambda: self.getLda())
        rbfMenu.add_command(label="4a. Train RBF Surrogate", command=lambda: self.trainRbf())
        rbfMenu.add_command(label="4b. Use existing RBF model",command = lambda: self.useExistingRbf())
        menubar.add_cascade(label='Surrogate Model',menu=rbfMenu)

##############################################################################
        # Set up monte carlo options menu
        mcMenu = Menu(self)

        mcMenu.add_checkbutton(label="General Uncertainty Analysis",onvalue=True, \
                               offvalue=False, variable=mcvar[0])
        mcMenu.add_checkbutton(label="Monte Carlo Analysis",onvalue=True, \
                               offvalue=False,variable=mcvar[1])
        menubar.add_cascade(label="UQ Methods",menu=mcMenu)

##############################################################################
        # Set up sensitivity method options menu
        saMenu = Menu(self)

        saMenu.add_checkbutton(label="(Local) Derivatives",onvalue=True, \
                               offvalue=False, variable=savar[0])
        saMenu.add_checkbutton(label="(Global) EFAST Total Effect Indices", onvalue=True, \
                               offvalue=False,variable=savar[1])
        saMenu.add_checkbutton(label="(Global) Monte Carlo First Order Effect Indices",onvalue=True, \
                               offvalue=False, variable=savar[2])
        saMenu.add_checkbutton(label="(Global) FAST First Order Effect Indices", \
                               onvalue=True,offvalue=False,variable=savar[4])
        saMenu.add_checkbutton(label="Sampling Scatter Plot Data", \
                               onvalue=True,offvalue=False,variable=savar[5])
        menubar.add_cascade(label="SA Methods",menu=saMenu)


##############################################################################
        # Set up extras menu
        exMenu = Menu(menubar)
        exMenu.add_command(label="Generate Default Files", \
                           command=lambda: self.gendefaults(self.wrkdir))
        exMenu.add_command(label="Quit",command=self.parent.destroy)
        menubar.add_cascade(label="Extras",menu=exMenu)

##############################################################################
        # Set up and pack setup info frame
        mcsetup = self.makeform(mcsFrame,self.mcsfields,self.mcsdefaults)
        fastsetup = self.makeform(FASTFrame,self.efastfields,self.efastdefaults)

##############################################################################        
        # Set up start and stop buttons
        b1 = tk.Button(buttonFrame,text='Start',font=self.customFont, \
                       command=lambda: self.startmc(self.wrkdir, self.paramfile,\
                       self.modelfile,mcvar,savar,povar,fastvar,mcsetup,fastsetup,\
                       plotsetup,plotvar))
        
        optionLabel = tk.Label(FASToptionframe)
        optionLabel["text"] = "Frequency Type"
        optionLabel.pack(side='left')
        fast_freq_type = tk.OptionMenu(FASToptionframe,fastvar[0],'Primes','Platonians')
        fast_freq_type.pack(side='top',padx=5,pady=5)
        FASToptionframe.pack(side='top')
        fast_shuffle_btn = tk.Checkbutton(FASTFrame,text="Shuffle Frequencies", onvalue=True, \
                                          offvalue=False,variable=fastvar[1])
        fast_shuffle_btn.pack(side='top',padx=5,pady=5)

##############################################################################
        # Set up plot options frames

        plotsetup = self.makeform(plotLeftFrame,('IndVar Label', 'Output Label'),('X','Y'))
        plotLeftFrame.pack(side='top')
        plotRightFrame.pack(side='top')
        swap_btn = tk.Checkbutton(plotRightFrame,text="Swap Axes",onvalue=True, \
                                  offvalue=False,variable=plotvar[0])
        lgx_btn = tk.Checkbutton(plotRightFrame,text="Log(X)",onvalue=True, \
                                 offvalue=False,variable=plotvar[1])
        lgy_btn = tk.Checkbutton(plotRightFrame,text="Log(Y)",onvalue=True, \
                                 offvalue=False,variable=plotvar[2])
        lgx_btn.pack(side='left',padx=5,pady=5)
        lgy_btn.pack(side='left',padx=5,pady=5)
        swap_btn.pack(side='left',padx=5,pady=5)


##############################################################################                                 
        # Start button setup
        b1.pack(side='right',padx=5,pady=5)

        #Pack frames
        baseFrame.pack(side='top',padx=5,pady=5)
        mcsFrame.pack(side='top',padx=5,pady=5)
        FASTFrame.pack(side='top',padx=5,pady=5)
        plotBaseFrame.pack(side='top',padx=5,pady=5)
        buttonFrame.pack(side='top',padx=5,pady=5)

##############################################################################

    def getMod(self):
        try:
            self.modelfile = tkFileDialog.askopenfilename(**self.mod_file_opt)
            print "\nModel file set as: %s" % self.modelfile
        except:
            print "\nNo model file selected..."        
    def getParam(self):
        try:
            self.paramfile = tkFileDialog.askopenfilename(**self.param_file_opt)
            print "\nParameter file set as: %s" % self.paramfile
        except:
            print "\nNo parameter file selected..."

    def getRbfParams(self):
        # Set RBF training points
        try:
            self.rbfTrainDir = tkFileDialog.askopenfilename(**self.param_file_opt)
            if os.path.isfile(self.rbfTrainDir):
                self.rbfTrainSet = True
                self.rbfTrain = np.genfromtxt(self.rbfTrainDir,delimiter=',',skip_header=1)
                print "\nRBF training points set as: %s" % self.rbfTrainDir
        except:
            print "\nNo RBF training points selected..."

    def getLda(self):
        try:
            self.rbfLda = tkFileDialog.askopenfilename(**self.param_file_opt)
            if os.path.isfile(self.rbfLda):
                self.rbfDataSet = True
                self.rbfData = np.genfromtxt(self.rbfLda,delimiter=',',skip_header=1)
                print "\nRBF training data set as: %s" % self.rbfLda
                setSurrogate = True
        except:
            print "\nNo RBF training data selected..."

    def trainRbf(self):
        if self.rbfTrainSet == False:
            print "Training points not set..."
            return 0
        if self.rbfDataSet == False:
            print "Training data not set..."
            return 0
        if self.rbfTrainSet == True and self.rbfDataSet == True:
            setSurrogate = True
            try:
                self.ldaMatrix = RBF.lda_matrix(self.rbfTrain,self.rbfData,self.rbfC,self.rbfType)
                steps = self.ldaMatrix.shape[1]
                print "RBF training successful..."
                print "\nSet 'Model Points' to %s" % steps
                self.genRbf()
                self.steps_from_rbf_train = steps
                #np.savetxt('uqsa_lda.csv',self.ldaMatrix,delimiter=',')
            except:
                print "RBF training failed. Check inputs..."

    def get_rbf_settings(self):
        #Parse file, find settings
        f = open(self.modelfile,'r')
        flines = f.readlines()
        clinestart = ['c = ','c=','c =','c= ']
        typestart = ['rbf_type = ','rbf_type=','rbf_type =','rbf_type= ']
        for line in flines:
            for cstart in clinestart:
                if line.startswith(cstart):
                    try:
                        cline = line.split(cstart)
                        self.rbfC = float(cline[1])
                    except:
                        print "Error reading 'c' value..."
                        self.rbfC = 1.0

            for tstart in typestart:
                if line.startswith(tstart):
                    try:
                        tline = line.split(tstart)
                        self.rbfType = int(tline[1])
                    except:
                        print "Error reading 'rbf_type' value..."
                        self.rbfType = 3

    def useExistingRbf(self):
        print "Using existing RBF.mdl file"
        if self.rbfTrainSet == False:
            print "Training points not set..."
            return 0
        if self.rbfDataSet == False:
            print "Training data not set..."
            return 0
        if self.rbfTrainSet == True and self.rbfDataSet == True:
            setSurrogate = True
            
            #ask for RBF file
            print "Detecting RBF settings..."
            self.modelfile = tkFileDialog.askopenfilename(**self.mod_file_opt)
            print "Model file set as: %s\n" %self.modelfile
            #look for rbf settings in model file
            self.get_rbf_settings()

            if self.rbfType == 1:
                typename = 'ThinPlate'
            elif self.rbfType == 2:
                typename = 'Gaussian'
            elif self.rbfType == 3:
                typename = 'Multiquadric'
            elif self.rbfType == 4:
                typename = 'InvMultiquadric'

            print "Detected RBF settings: c=%s , type=%s (%s)" %(self.rbfC, self.rbfType,typename)
            print "Training RBF model..."
            self.ldaMatrix = RBF.lda_matrix(self.rbfTrain,self.rbfData,self.rbfC,self.rbfType)
            steps = self.ldaMatrix.shape[1]
            print "RBF training successful..."
            print "\nSet 'Model Points' to %s" %steps
            self.steps_from_rbf_train = steps
            
	
    def copyfile(self,src,dest):
        try:
            shutil.copy(src,dest)
        except shutil.Error as e:
            print('Error: %s' % e)
        except IOError as e:
            print('Error: %s' % e.strerror)

def main():
    root = Tk()
    win = MCGUI(root)
    root.geometry("425x500+0+0")
    root.resizable(width=True, height=True)
    root.mainloop()


if __name__ == '__main__':
    main()

# Copyright (c) 2015 Justin Hughes (jhughes@cavs.msstate.edu)

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

CAVS-UQSA: Uncertainty Quantification and Sensitivity Analysis Routines
	for engineering models


Lead: Justin Hughes (justin.m.hughes@outlook.com) (Python)
Developer: William Williams (wrw104@msstate.edu) (Python)
Developer: William Lawrimore III (Matlab)

Required packages:
    NumPy 			http://sourceforge.net/projects/numpy/files/
    MatPlotLib	 		http://matplotlib.org/downloads.html
    SciPy 			http://www.scipy.org/install.html

	
Linux:
	sudo apt-get install numpy
	sudo apt-get install scipy
	sudo apt-get install matplotlib

Features:
    GUI interface for Monte Carlo analysis setup....................(90%)
    Monte Carlo random sampling method..............................(100%)
        MC method for uncertainty propagation
        MC method for first order effect indices
    Sigma-Normalized Local Sensitivity Analysis.....................(100%)
    Coleman/Steele Propagation method...............................(100%)
    Fourier Amplitude Sensitivity Testing (FAST)....................(100%)
        First order effect indices for Global Sensitivity Analysis
    (WIP) Total Effect Indices for Global Sensitivity Analysis......(10%)
    (WIP) Histogram-based analysis of output data to determine......(60%)
            statistical PDF with least error utilizing Maximum      
            Likelihood Estimation (MLE) routines
    (WIP) Plot output...............................................(40%)
    (WIP) Other features on request

How to use:
    1) Select working directory.
    2) Select model file.
    3) Select parameters file.
    4) Select uncertainty method
    5) Select sensitivity method
    6) Hit start. Process is shifted to the command prompt/terminal and handled automatically.
	
	If a model format file or parameters file is needed use "Extras -> Generate Default Files".
	Files are output to the working directory (if set), default output location is the same
	directory as UQSA.py.
   
--- UQSA ---


-- UQSA.py                           - Python GUI script
-- /model_library
   |-- simple_expoenential.txt	     - Default model file for testing script
-- /parameter_library
   |-- simple_exponential_params.csv - Default parameters file
-- /src
   |-- __init__.pyc                  - Allows for the calling of custom modules from /src
   |-- main.pyc                          - Main analysis entry point.
   |-- sensitivity_analysis.pyc      - Houses local/global sensitivity analysis routines
   |-- histgen.pyc		     - Custom hitogram analysis script, provides more control
   |                                   for data analysis
   |-- user_model_wrapper.pyc        - Wrapper module to mesh parameter file with model file,
   |				       generates usable python model code in /src
   |-- unc_propagation.pyc	     - Coleman and Steele propagation method script

'''

Copyright (c) 2016 Justin M. Hughes

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''
from __future__ import division
try:
	import user_model as mdl
except:
	print "user_model.py not found..."
#import user_model_EFAST as mdlfast
import modplot as mp
import numpy as np
import os,math,random,re
from terminal_progress_bar import progress_bar
from histgen import fasthist

try:
    from user_model import rejection_method
    user_rejection_method = True
except:
    user_rejection_method = False

def perturb(i,props,pct=0.0001):
    pprops = np.empty(len(props),dtype=float)*0
    for j  in range(0,len(pprops)):
        pprops[j]= props[j]

    pprops[i] = pprops[i]*(1+pct)
    return pprops

#def vertical_savetxt(filename,data,delimiter=','):
#    return 0

def isPrime(n):
    return re.match(r'^1?$|^(11+?)\1+$','1' * n) == None

def prime_numbers(N=5,minimum=8):
    # Returns N prime numbers
    M = 100
    while True:
        nums = list()
        while len(nums) < N:
            nums += filter(isPrime, range(minimum,M))
            M += 500
        if len(nums) != len(set(nums)):
            duplicates = True
        else:
            break
    return nums[:N] 

def platonions(N):
    # https://zulko.wordpress.com/tag/mathematics-incommensurate-numbers-incommensurable-platonions/
    """ Returns numbers of the form sqrt(I) where I is not a
    multiple of a square. Such numbers are incommensurable."""
    squareMultiples = [k*(a**2) for a in range(2,int(math.sqrt(N))+2)
                                for k in range(1,1+int((N+1)/(a**2)))]
    return [math.sqrt(i) for i in range(10,N+10) if not i in squareMultiples]

def findIncommensurates(K):
    # https://zulko.wordpress.com/tag/mathematics-incommensurate-numbers-incommensurable-platonions/
    return platonions(int(K/0.60))[:K]

def search_curve_uniform(minimum,maximum,omega,ssmin,ssmax,Nsamples,rand_phase=0.0,rand_angle='True'):
    # See reference. This function is a single parameter search curve used by Saltelli
    #   et al 1999. When sampled, this function closely returns a uniform distribution.
    #   This version has been abstracted to return values between a user defined maximum
    #   and minimum. Original version was used to sample from a unit hypercube. To aid
    #   with better input space sampling, a random phase angle is added (eqn 20 of
    #   included reference.
    '''
    Saltelli, A., Tarantola, S., Chan, K.P.-S., "A Quantitative Model-Independent
    Method for Global Sensitivity Analysis of Model Output", Technometrics, Vol 41,
    No 1, Feb 1999.
    '''
    xss = np.zeros((Nsamples+1,1),dtype=float)
    
    xm = maximum - minimum
    xa = minimum

    # Switch for enabling/disabling random phase angle in search curve
    if rand_angle == 'True':
        include_rand = 1
    else:
        include_rand = 0
    
    dss = (ssmax-ssmin)/Nsamples
    ss = [ssmin + i*dss for i in range(0,Nsamples+1)]
    xss = [xm*(0.5+np.arcsin(np.sin(omega*sval + include_rand*rand_phase))/np.pi)+xa for sval in ss]
    
    return ss, xss

def randprops(props,stdevs,dist):
    rand = props-props
    for i in range(0,len(props)):
        if dist[i] == 0: # NORMAL DISTRIBUTION
            if stdevs[i] < 0:
                rand[i] = float(np.random.normal(-1.0*props[i],-1.0*stdevs[i],1))
                rand[i] = -1.0*rand[i]
            if stdevs[i] == 0:
                rand[i] = float(props[i])
            if stdevs[i] > 0:
                rand[i] = float(np.random.normal(props[i],stdevs[i],1))
        if dist[i] == 1: # UNIFORM DISTRIBUTION
            if stdevs[i] == 0:
                rand[i] = float(props[i])
            if stdevs[i] < 0:
                rand[i] = float(np.random.uniform(np.abs(props[i])-np.abs(stdevs[i]),np.abs(props[i])+np.abs(stdevs[i]),1))
                rand[i] = -1.0*rand[i]
            if stdevs[i] > 0:
                rand[i] = float(np.random.uniform(props[i]-stdevs[i],props[i]+stdevs[i],1))
        if dist[i] == 2: # LOGNORMAL DISTRIBUTION (CURRENTLY DOES NOT WORK, may have to create custom inverse lognormal function)
            if stdevs[i] < 0:
                rand[i] = float(np.random.lognormal(-1.0*props[i],-1.0*stdevs[i],1))
                rand[i] = -1.0*rand[i]
            if stdevs[i] == 0:
                rand[i] = float(props[i])
            if stdevs[i] > 0:
                rand[i] = float(np.random.lognormal(props[i],stdevs[i],1))
    return rand

def rand_single(i,propvec,stdevs,dist):
    rand = np.zeros(len(propvec))
    rand += propvec 
    if dist[i] == 0: # NORMAL DISTRIBUTION
        if stdevs[i] < 0:
            rand[i] = float(np.random.normal(-1.0*propvec[i],-1.0*stdevs[i],1))
            rand[i] = -1.0*rand[i]
        if stdevs[i] == 0:
            rand[i] = float(propvec[i])
        if stdevs[i] > 0:
            rand[i] = float(np.random.normal(propvec[i],stdevs[i],1))
    if dist[i] == 1: # UNIFORM DISTRIBUTION
        if stdevs[i] == 0:
            rand[i] = float(propvec[i])
        if stdevs[i] < 0:
            rand[i] = float(np.random.uniform(np.abs(propvec[i])-np.abs(stdevs[i]),np.abs(props[i])+np.abs(stdevs[i]),1))
            rand[i] = -1.0*rand[i]
        if stdevs[i] > 0:
            rand[i] = float(np.random.uniform(propvec[i]-stdevs[i],propvec[i]+stdevs[i],1))
    if dist[i] == 2: # LOGNORMAL DISTRIBUTION (CURRENTLY DOES NOT WORK, may have to create custom inverse lognormal function)
        if stdevs[i] < 0:
            rand[i] = float(np.random.lognormal(-1.0*propvec[i],-1.0*stdevs[i],1))
            rand[i] = -1.0*rand[i]
        if stdevs[i] == 0:
            rand[i] = float(propvec[i])
        if stdevs[i] > 0:
            rand[i] = float(np.random.lognormal(propvec[i],stdevs[i],1))
    return rand

def rand_single_prop(prop,stdev,dist):
    # Returns random value of a single parameter
    rand = 0
    if dist == 0: #NORMAL DISTRIBUTION
        if stdev < 0:
            rand = float(np.random.normal(np.abs(prop),np.abs(stdev),1))
            rand = -1.0*rand
        if stdev == 0:
            rand = float(prop)
        if stdev > 0:
            rand = float(np.random.normal(prop,stdev,1))
    if dist == 1: #UNIFORM DISTRIBUTION
        if stdev == 0:
            rand = float(prop)
        if stdev < 0:
            rand = float(np.random.uniform(np.abs(prop)-np.abs(stdev),np.abs(prop)+np.abs(stdev),1))
            rand = -1.0*rand
        if stdev > 0:
            rand = float(np.random.uniform(prop-stdev,prop+stdev,1))
    return rand

def sens_local_normed(xmin,xmax,Nsteps,lda,xtrain,props,instdev,mcstdev,paramlist='No Header',descriptor='',mc_performed='False'):
    # Local sensitivity analysis method
    # print paramlist,descriptor
#    print instdev.shape, mcstdev.shape
    paramlist = 'X,'+','.join(paramlist)
    err = 'sens_local_normed: Clear'
#    num_sim_levels = len(mcstdev)
    num_params = len(props)
    work_dir = os.getcwd()
    os.mkdir('Local_Sensitivity_Analysis')
    os.chdir('Local_Sensitivity_Analysis')
    local_sensitivity = np.zeros([Nsteps+1,num_params],dtype=float)
    #print props
    pct = 0.0001

    for j in range(0,num_params):
        xx, yu, reject = mdl.model(xmin,xmax,Nsteps,perturb(j,props,pct),lda,xtrain)
        xx, yd, reject = mdl.model(xmin,xmax,Nsteps,perturb(j,props,-1.0*pct),lda,xtrain)
        denom = 2*pct*props[j]
        local_sensitivity[:,j] = abs((yu.T - yd.T)/denom)

    sensout = np.hstack((xx,local_sensitivity))
    np.savetxt("DERIVATIVES.csv",sensout,delimiter=',', \
               header=paramlist,fmt='%10.15f')

    # If MC performed, normalize derivatives
    if mc_performed == 'True':
        for i in range(0,len(mcstdev)):
            for j in range(0,num_params):
                local_sensitivity[i,j] = local_sensitivity[i,j]*instdev[j]/mcstdev[i]
        sensout = np.hstack((xx,local_sensitivity))
        np.savetxt("MC_NORM_SENSITIVITY.csv",sensout,delimiter=',', \
                   header=paramlist,fmt='%10.15f')

    os.chdir(work_dir)
    return err

def sens_global_vratio(xmin,xmax,Nsteps,lda,xtrain,simpoints,props,instdev,disttype,indvar,paramlist='No Header'):
    # First order effect index
    # Determines variance of the model when subjected to variations of a single parameter
    work_dir = os.getcwd()
    os.mkdir("MC_First_Order_Effect_Indices")
    os.chdir("MC_First_Order_Effect_Indices")
    err = 'sens_local_foei: Clear'
    print "Running model simulations. Please wait..."
    header = 'X,'+ ','.join(paramlist)
    sensdata = np.zeros((Nsteps+1,simpoints,len(props)),dtype=float)
    propvar = np.zeros((Nsteps+1,len(props)),dtype=float)
    foei = np.zeros((Nsteps+1,len(props)),dtype=float)
    modout = np.zeros([Nsteps+1,simpoints],dtype=float)
    modvar = np.zeros((Nsteps+1,1),dtype=float)
    avgfoei = np.zeros((len(props),1),dtype=float)

    
    progress_bar(0,simpoints,'MCSRS')
    for j in range(0,simpoints):
        rand_props = randprops(props,instdev,disttype)
        #print rand_props.shape
        xx, yy, reject = mdl.model(xmin,xmax,Nsteps,rand_props,lda,xtrain)
        modout[:,j] = yy.T
        progress_bar(j,simpoints,'MCSRS')
    progress_bar(simpoints,simpoints,'MCSRS')
    
    print "Calulating total model variance..."
    for i in range(0,Nsteps+1):
        modvar[i] = np.var(modout[i,:])
    
    print "Starting single parameter analysis..."
    # Vary single parameter at a time
    progress_bar(0,len(props),'MCSRS          ')
    for k in range(0,len(props)):
        #Skip parameter if it does not vary
        if instdev[k] == 0:
            continue
        #Skip independent variable
        if paramlist[k] == indvar:
            continue
        progress_bar(k, len(props),'%s MCSRS          ' %paramlist[k])
        # Gather simulations, analyze variance by row

        for j in range(0,simpoints):
            randsingleprop = rand_single(k,props,instdev,disttype)
            xx, yy, reject = mdl.model(xmin,xmax,Nsteps,randsingleprop,lda,xtrain)
            sensdata[:,j,k] = yy.T
        for i in range(0,Nsteps+1):
            propvar[i,k] = np.var(np.double(sensdata[i,:,k]))
            # Get variance input/output variance ratio
#            foei[i,k] = abs(propvar[i,k]/sum(propvar[i,:]))
            if modvar[i] != 0:
                foei[i,k] = abs(np.double(propvar[i,k]/modvar[i]))
    progress_bar(10,10,'MCSRS          ')
    # normalize foei by row
    #for i in range(0,Nsteps+1):
    #    varsum = math.fsum(foei[i,:])
    #    for k in range(0,len(props)):
    #        if varsum !=0:
    #            foei[i,k] = np.double(foei[i,k]/varsum)
    #        else:
    #            foei[i,k] = 'nan'
                
    # Average FOEI for each parameter
    for k in range(0,len(props)):
        avgfoei[k] = np.mean(foei[:,k])
        
    outdata = np.hstack((xx,foei))
    np.savetxt("MC_First_Order_Effect_Indices.csv",outdata,delimiter=',', \
               header=header,fmt='%10.15f')
    outdata = np.hstack((xx,propvar))
    np.savetxt("Model_variances_by_parameter.csv",outdata,delimiter=',', \
               header=header,fmt='%10.15f')
    outdata = np.hstack((xx,modvar))
    np.savetxt("Model_total_variance.csv",outdata,delimiter=',', \
               header="X,Mod Var",fmt='%10.15f')
    np.savetxt("MC_Average_First_Order_Effect_Indices.csv",avgfoei.T, \
               delimiter=',',header=','.join(paramlist),fmt='%10.15f')
    os.chdir(work_dir) 
    return err

def integrate_simpsons(data,a,b,n):
    # Simpsons rule for discrete numerical integration
    # h: step size
    # data: function data for integration, single dataset/vector
    # n: number of function evaluations
    data = list(data)
    # step size
    h = float((b-a)/n)

    # First term
    s = (data[0]+data[-1])

    # Odd terms
    for i in range(1,n,2):
        s += 4.0*data[i]

    # Even terms
    for i in range(2,n,2):
        s += 2.0*data[i]
    
    return s*h/3.0

def fourier_A(gg,omega,p,smin,smax,Nsamples):
    # g: data from search curve execution of model
    # omega: parameter freqency
    # p: resonance frequency multiplier
    # n: number of search curve samples
    dss = (smax-smin)/Nsamples
    ss = [smin + i*dss for i in range(0,Nsamples+1)]
    cos_of_ss = [np.cos(p*omega*ss[i]) for i in range(0,Nsamples+1)]
    dAA = [gg[i]*cos_of_ss[i] for i in range(0,Nsamples+1)]

    return integrate_simpsons(dAA,smin,smax,len(dAA))/(2.0*np.pi)

def fourier_B(gg,omega,p,smin,smax,Nsamples):
    # g: data from search curve execution of model
    # omega: parameter freqency
    # p: resonance frequency multiplier
    # n: number of search curve samples
    dss = (smax-smin)/Nsamples
    ss = [smin + i*dss for i in range(0,Nsamples+1)]
    sin_of_ss = [np.sin(p*omega*ss[i]) for i in range(0,Nsamples+1)]
    dBB = [gg[i]*sin_of_ss[i] for i in range(0,Nsamples+1)]
     
    return integrate_simpsons(dBB,smin,smax,Nsamples+1)/(2*np.pi)

def sens_global_FAST(xmin,xmax,Nsteps,lda,xtrain,props,instdev,disttype,num_samples,resamples,freq_type=0,shuffle_freqs='True',paramlist='No Header'):
    #Fourier Amplitude Sensitivity Test (FAST) method to calculate main effect indices
    # ANOVA-like decomposition of model output variance
    err = "sens_global_FAST: Clear"
    wrkdir = os.getcwd()
    os.mkdir("FAST_First_Order_Effect_Indices")
    os.chdir("FAST_First_Order_Effect_Indices")
    fastdir = os.getcwd()
    
    num_params = len(props)
    num_fourier_terms = 20
    header = 'X,'+ ','.join(paramlist)
    
    # Get incommensurate frequencies and shuffle (for randomness)
    print "Generating incommensurate frequencies..."

    if freq_type == "primes" or freq_type == "Primes":
        print "Using Prime numbers for frequencies..."
        freqs = prime_numbers(num_params,8)
    else:
        print "Using Platonians for frequencies..."
        freqs = findIncommensurates(num_params)

    if shuffle_freqs == 'True':
        print "Shuffling frequencies for added randomness..."
        random.shuffle(freqs)
 

	# Check user number of samples input versus Nyquist theorem
    if (32*int(math.ceil(max(freqs))) + 8) > num_samples:
        num_samples = 32*int(math.ceil(max(freqs))) + 8
    # Force function samples even (simpsons rule requires even samples)    
    if num_samples%2 == 1:
        num_samples+=1

    print "Using %s samples from search curve..." % num_samples  

    Sensitivity = np.zeros((Nsteps+1,num_params),dtype=float)

    for rs in range(0,resamples):
        print("\nSearch curve sample %s" %(rs+1))
        # Generate random phase angles for search curves
        phase_angle = np.random.uniform(0.0,2.0*np.pi,num_params)
        
        # Generate input matrix using search curves for each parameter
        input_space = np.zeros((num_samples+1,num_params),dtype=float)
        output_space = np.zeros((Nsteps+1,num_samples+1),dtype=float)
    
        # Storage variables for fourier coefficients
        AA = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
        BB = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
        dAA = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
        dBB = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
        DD_by_param = np.zeros((Nsteps+1,num_params),dtype=float)
        DD_sum = np.zeros((Nsteps+1,1),dtype=float)
        DD = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
        rfreqs = np.zeros((num_fourier_terms+1,num_params),dtype=float)
        
        avg_sens = np.zeros((1,num_params),dtype=float)
        
        # set limits for search curve
        # curve is periodic over the interval (0, 2*pi)
        ssmin = -1.0*np.pi
        ssmax = np.pi
        print "Generating input space..."
        for i in range(0,num_params):
            progress_bar(i,num_params,'%s      ' %paramlist[i])
            if disttype[i] == 0:
                param_min = props[i] - 2.0*instdev[i]
                param_max = props[i] + 2.0*instdev[i]
            elif disttype[i] == 1:
                param_min = props[i] - instdev[i]
                param_max = props[i] + instdev[i]
            else:
                print 'Incorrect distribution type: (0 or 1)'
                err = 'FAST: Incorrect distribution type: %s' %disttype[i]
                return err
    
            if instdev[i] != 0:
                ss, input_space[:,i] = search_curve_uniform(param_min,param_max,freqs[i],ssmin,ssmax,num_samples,phase_angle[i],'True')
            else:
                input_space[:,i] = props[i]
        progress_bar(10,10,'Done          ')
        print "Evaluating model input space..."
        for i in range(0,num_samples+1):
            progress_bar(i,num_samples+1,'FAST Model Eval')
            xx, yy, reject = mdl.model(xmin,xmax,Nsteps,input_space[i,:],lda,xtrain)
            output_space[:,i] = yy.T
        progress_bar(10,10,'Done           ')
    
        num_terms = 5
        #Filter NaN values, replace with local average
        for i in range(0,num_samples+1):
            for j in range(0, output_space.shape[0]):
                if str(output_space[j,i]) == 'nan':
                    #print "NaN in output space!"
                    # Check if near the ends of the dataset
                    iback = i - num_terms
                    if iback < 0:
                        iback = 0
                    iforward = i + num_terms
                    if iforward > output_space.shape[1]-1:
                        iforward = output_space.shape[1]-1
                    output_space[j,i] = np.nanmean(output_space[j,iback:iforward])
        #np.savetxt('FAST_outputs_debug.csv',output_space,delimiter=',',fmt='%10.15f')
                    # Estimate the total variance (Saltelli et al 1999, eq 10)

        # Calculate Fourier coefficients
        print "Calculating Fourier series coefficients..."
        
        for i in range(0,Nsteps+1):
            progress_bar(i,Nsteps+1,'FAST Coeffs')
            for k in range(0,num_params):
                for j in range(0,num_fourier_terms+1):
                    AA[i,j,k] = fourier_A(output_space[i,:],freqs[k],j+1,ssmin,ssmax,num_samples)
                    BB[i,j,k] = fourier_B(output_space[i,:],freqs[k],j+1,ssmin,ssmax,num_samples)
                    DD[i,j,k] = 2.0*AA[i,j,k]**2.0 + 2.0*BB[i,j,k]**2.0
        progress_bar(10,10,'Done       ')
        
        #os.mkdir("Spectrum_Data")
        #os.chdir("Spectrum_Data")
        #specdata = os.getcwd()
        #os.mkdir("Spectrum_by_parameter")
        #os.chdir("Spectrum_by_parameter")
        
        #print "Writing spectrum data to file..."
        #print "Writing spectrumm data for each parameter..."
        #step_count = list([str(i) for i in range(0,Nsteps+1)])
        #spectrum_header = "Frequency," + ','.join(step_count)
        #for k in range(0,num_params):
            #resonants = [(i+1)*freqs[k] for i in range(0,num_fourier_terms+1)]
            #resonants = np.array(resonants).reshape(len(resonants),1)
            #rfreqs[:,k] = resonants[:,0]
            #writename = str(paramlist[k])+ "_FAST_Acoeff_spectrum.csv"
            #np.savetxt(writename,np.hstack((resonants,AA[:,:,k].T)),delimiter=',',header=spectrum_header,fmt='%10.15f')
            #writename = str(paramlist[k])+ "_FAST_Bcoeff_spectrum.csv"
            #np.savetxt(writename,np.hstack((resonants,BB[:,:,k].T)),delimiter=',',header=spectrum_header,fmt='%10.15f')
            #writename = str(paramlist[k])+"_FAST_Dcoeff_spectrum.csv"
            #np.savetxt(writename,np.hstack((resonants,DD[:,:,k].T)),delimiter=',',header=spectrum_header,fmt='%10.15f')
        
        #os.chdir(specdata)
    
        # Square each element of AA and BB prior to summation
        AA = AA*AA
        BB = BB*BB
      
        print "Calculating FAST sensitivity measures..."
        # Get summation of fourier terms by parameter for sensitivity breakdown
        for i in range(0,Nsteps+1):
            for k in range(0,num_params):
                DD_by_param[i,k] = 2.0*np.sum(AA[i,:,k]) + 2.0*np.sum(BB[i,:,k])
            DD_sum[i] = np.sum(DD_by_param[i,:])

        #print "Writing spectrum data for each step..."
        #os.mkdir('Spectrum_by_step')
        #os.chdir('Spectrum_by_step')
        #specdir = os.getcwd()
        #os.mkdir('PLOTS')
        #for i in range(0,Nsteps+1):
            #rfreqvec = rfreqs.T.reshape(rfreqs.size,1)
            #ddvec = DD[i,:,:].T.reshape(DD[i,:,:].size,1)
            #maxD = np.nanmax(ddvec)
            #writename = str('Step%s'%i)+"_FAST_spectrum"
            #outdata = np.hstack((rfreqvec,ddvec/maxD))
            #outdata = np.array(sorted(outdata,key=lambda x: x[0]))
            #np.savetxt(writename+'.csv',outdata,delimiter=',',fmt='%10.15f')
            #os.chdir('PLOTS')
            #mp.modplot((outdata[:,0],outdata[:,1]),('Frequency Spectrum: Step %s' %i),'r-',writename,'X','Normalized Dcoeff','False','False','False')
            #os.chdir(specdir)
        #os.chdir(fastdir)
        #Calculate sensitivity ratios
        for i in range(0,Nsteps+1):
            for k in range(0,num_params):
                Sensitivity[i,k] += DD_by_param[i,k]/DD_sum[i]
                #Sensitivity[i,k] += DD_by_param[i,k]/D_tot[i]
                # If parameter doesn't vary, make its sensitivity null
                if instdev[k] == 0:
                    Sensitivity[i,k] = 0

    Sensitivity = Sensitivity/resamples
    # Write parameter list and associated frequencies from spectral analysis
    summary_file = open('FAST_Parmeter_Frequencies.csv','w')
    summary_file.write('Parameter, FAST Frequency\n')
    for i in range(0,num_params):
        summary_file.write(paramlist[i] + ',' + str(freqs[i]) +'\n')       
    summary_file.close()
        
#    np.savetxt("DD_by_param_DEBUG.csv",np.hstack((xx,DD_by_param)),delimiter=',',header=header)
    np.savetxt("FAST_First_Order_Indices.csv",np.hstack((xx,Sensitivity)),delimiter=',',header=header,fmt='%10.15f')
    os.chdir(wrkdir)
        
    return err

def sample_calc(rs,om):
    rad = np.sqrt(rs**2.0 + om**2.0)
    return int(math.ceil(22.0*rad-488.0))
    

def sens_global_EFAST(xmin,xmax,Nsteps,lda,xtrain,props,instdev,disttype,num_samples,resamples,freq_type=0,shuffle_freqs='True',paramlist='No Header'):
    #Extended Fourier Amplitude Sensitivity Test (FAST) method to calculate total effect indices
    # ANOVA-like decomposition of model output variance
    err = "sens_global_EFAST: Clear"
    wrkdir = os.getcwd()
    os.mkdir("EFAST_Total_Effect_Indices")
    os.chdir("EFAST_Total_Effect_Indices")
    fastdir = os.getcwd()
    
    num_params = len(props)
    num_fourier_terms = 20
    header = 'X,'+ ','.join(paramlist)
    
    # Generate set of similar frequencies for omega_not_i
    print("Generating set of similar frequencies...")
    
    step = 3
    omega = [3+i*step for i in range(0,num_params)]

    # reuse about half the generated frequencies
    i = 0
    while i < num_params/2:
        omega[-1-i] = omega[i]
        i += 1
    
    omega_min = 32*max(omega)
    omega_diff = prime_numbers(1,omega_min)[0]
    #print(max(omega),omega_diff)

    if num_samples < 32*omega_diff + 8:
        num_samples = 32*omega_diff + 8

    if num_samples < sample_calc(resamples,omega_diff):
        num_samples = sample_calc(resamples,omega_diff)
    #print 32*omega_diff+8, sample_calc(resamples,omega_diff)
    # Force function samples even (simpsons rule requires even samples)    
    if num_samples%2 == 1:
        num_samples+=1
    
    print("Generating incommensurate frequencies...")
    if freq_type == "primes" or freq_type == "Primes":
        print "Using Prime numbers for frequencies..."
        ifreqs = prime_numbers(num_params,8)
    else:
        print "Using Platonians for frequencies..."
        ifreqs = findIncommensurates(num_params)

    if shuffle_freqs == 'True':
        print "Shuffling frequencies for added randomness..."
        random.shuffle(ifreqs)
    
    fastsamples = 32*int(math.ceil(max(ifreqs)))+8
    if fastsamples%2 == 1:
        fastsamples += 1

    print("Using %s samples from search curve..." %num_samples)
    print("Using %s samples for variance estimation..." %fastsamples)
    
    input_space = np.zeros((num_samples+1,num_params),dtype=float)
    output_space = np.zeros((Nsteps+1,num_samples+1),dtype=float)
    
    finput_space = np.zeros((fastsamples+1,num_params),dtype=float)
    foutput_space = np.zeros((Nsteps+1,fastsamples+1),dtype=float)
    
    # set limits for search curve
    # curve is periodic over the interval (0, 2*pi)
    ssmin = -1.0*np.pi
    ssmax = np.pi
    if resamples < 1:
        resamples = 1

    sens_storage = np.zeros((Nsteps+1,num_params),dtype=float)

    for ip in range(0,num_params):
        freqs = [om for om in omega]
        freqs[ip] = omega_diff

        print("\nComputing Total Effect Index: %s" %paramlist[ip])
        for jr in range(0,resamples):
            if jr == 0:
                print("Search curve sample %s for paramter %s" %(jr+1,paramlist[ip]))
            else:
                print("\nSearch curve sample %s for parameter %s" %(jr+1,paramlist[ip]))
            # initialize storage arrays inside loop to re-zero on each resample
            AA = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
            BB = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
            dAA = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
            dBB = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
            DD_by_param = np.zeros((Nsteps+1,num_params),dtype=float)
            DD_sum = np.zeros((Nsteps+1,1),dtype=float)
            DD = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
            Sensitivity = np.zeros((Nsteps+1,num_params),dtype=float)

            fAA = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
            fBB = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
            fdAA = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
            fdBB = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)
            fDD_by_param = np.zeros((Nsteps+1,num_params),dtype=float)
            fDD_sum = np.zeros((Nsteps+1,1),dtype=float)
            fDD = np.zeros((Nsteps+1,num_fourier_terms+1,num_params),dtype=float)

            # Generate random phase angles for each resampling of search curves
            phase_angle = np.random.uniform(0.0,2.0*np.pi,num_params)

            # Estimate total variance with FAST method
            print("Begin estimating total variance via FAST method...")
            print("Generating input space for total variance estimation...")
            for i in range(0,num_params):
                progress_bar(i,num_params,'%s          ' %paramlist[i])
                if disttype[i] == 0:
                    param_min = props[i] - 2.0*instdev[i]
                    param_max = props[i] + 2.0*instdev[i]
                elif disttype[i] == 1:
                    param_min = props[i] - instdev[i]
                    param_max = props[i] + instdev[i]
                else:
                    print 'Incorrect distribution type: (0 or 1)'
                    err = 'FAST: Incorrect distribution type: %s' %disttype[i]
                    return err

                if instdev[i] != 0:
                    ss, finput_space[:,i] = search_curve_uniform(param_min,param_max,ifreqs[i],ssmin,ssmax,fastsamples,phase_angle[i],'True')
                else:
                    finput_space[:,i] = props[i]
            progress_bar(1,1,'Done          ')

            print("Evaluating input space...")
            for i in range(0,fastsamples+1):
                progress_bar(i,fastsamples+1,'FAST Model Eval')
                xx, yy, reject = mdl.model(xmin,xmax,Nsteps,finput_space[i,:],lda,xtrain)
                foutput_space[:,i] = yy.T
            progress_bar(1,1,'Done            ')

            num_terms = 5
            #Filter NaN values, replace with local average
            for i in range(0,fastsamples+1):
                for j in range(0,foutput_space.shape[0]):
                    if str(foutput_space[j,i]) == 'nan':
                        iback = i-num_terms
                        if iback < 0:
                            iback = 0
                        iforward = i+num_terms
                        if iforward > foutput_space.shape[1]-1:
                            iforward = foutput_space.shape[1]-1
                        foutput_space[j,i] = np.nanmean(foutput_space[j,iback:iforward])

            # Calculate FAST coefficients
            print "Calculating Fourier series coefficients..."
            count = 0
            for i in range(0,Nsteps+1):
                for k in range(0,num_params):
                    for j in range(0,num_fourier_terms+1):
                        progress_bar(count,(Nsteps+1)*num_params*(num_fourier_terms+1),'FAST Coeffs')
                        fAA[i,j,k] = fourier_A(foutput_space[i,:],ifreqs[k],j+1,ssmin,ssmax,fastsamples)
                        fBB[i,j,k] = fourier_B(foutput_space[i,:],ifreqs[k],j+1,ssmin,ssmax,fastsamples)
                        fDD[i,j,k] = 2.0*fAA[i,j,k]**2.0 + 2.0*fBB[i,j,k]**2.0
                        count += 1
            progress_bar(10,10,'Done       ')
            
            #Square each element of fAA and fBB prior to summation
            fAA = fAA*fAA
            fBB = fBB*fBB

            print "Estimating total variance..."
            for i in range(0,Nsteps+1):
                for k in range(0,num_params):
                    fDD_by_param[i,k] = 2.0*np.sum(fAA[i,:,k]) + 2.0*np.sum(fBB[i,:,k])
                fDD_sum[i] = np.sum(fDD_by_param[i,:])

##############################################################################################################################################
            # Begin total sensitivity index calculations
##############################################################################################################################################            

            print("Generating input space for total effect indices...")
            for i in range(0,num_params):
                progress_bar(i,num_params,'%s          ' %paramlist[i])
                if disttype[i] == 0:
                    param_min = props[i] - 2.0*instdev[i]
                    param_max = props[i] + 2.0*instdev[i]
                elif disttype[i] == 1:
                    param_min = props[i] - instdev[i]
                    param_max = props[i] + instdev[i]
                else:
                    print "Incorrect distribution type: (0 or 1)"
                    err += '\nEFAST: Incorrect distribution type: %s' %disttype[i]
                    return err

                if instdev[i] != 0:
                    ss, input_space[:,i] = search_curve_uniform(param_min,param_max,freqs[i],ssmin,ssmax,num_samples,phase_angle[i],'True')
                else:
                    input_space[:,i] = props[i]
            progress_bar(1,1,'Done          ')
            
            print("Evaluating model input space...")

            for i in range(0,num_samples+1):
                progress_bar(i,num_samples+1,'EFAST Model Eval')
                xx, yy, reject = mdl.model(xmin,xmax,Nsteps,input_space[i,:],lda,xtrain)
                output_space[:,i] = yy.T
            progress_bar(1,1,'Done                    ')

            num_terms = 5
            # Filter NaN values, replace with local average
            for i in range(0,num_samples+1):
                for j in range(0, output_space.shape[0]):
                    if str(output_space[j,i]) == 'nan':
                        # Check if near the ends of the dataset
                        iback = i - num_terms
                        if iback < 0:
                            iback = 0
                        iforward = i + num_terms
                        if iforward > output_space.shape[1]-1:
                            iforward = output_space.shape[1]-1
                        output_space[j,i] = np.nanmean(output_space[j,iback:iforward])


            # Calculate Fourier coefficients
            print("Calculating Fourier series coefficients...")
            count = 0
            for i in range(0,Nsteps+1):
                for k in range(0,num_params):
                    for j in range(0,num_fourier_terms+1):
                        progress_bar(count,(Nsteps+1)*num_params*(num_fourier_terms+1),"Fourier Coeff Eval")
                        AA[i,j,k] = fourier_A(output_space[i,:],freqs[k],j+1,ssmin,ssmax,num_samples)
                        BB[i,j,k] = fourier_B(output_space[i,:],freqs[k],j+1,ssmin,ssmax,num_samples)                        
                        DD[i,j,k] = 2.0*AA[i,j,k]**2.0 + 2.0*BB[i,j,k]**2.0
                        count += 1
            progress_bar(10,10,'Done              ')  

            #Square each element of AA and BB prior to summation
            AA = AA*AA
            BB = BB*BB
           
            # Get summation of fourier terms, excluding freqs associated with ith parameter
            for i in range(0,Nsteps+1):
                for k in range(0,num_params):
                        DD_by_param[i,k] = 2.0*np.sum(AA[i,:,k]) + 2.0*np.sum(BB[i,:,k])

            #f = open("%s_%s_debug.csv" %(paramlist[ip],jr+1),'w')
            #f.write("DD_by_param, fDD_sum\n")
            
            for i in range(0,Nsteps+1):
                #f.write("%s, %s\n" %(DD_by_param[i,ip],float(fDD_sum[i])))
                Sensitivity[i,ip] = DD_by_param[i,ip]/fDD_sum[i]
            #f.close()
            sens_storage[:,ip] += Sensitivity[:,ip]
            
            # Go to next resample
    print("Calculating Total Effect Indices...")
    # divide by number of resamples for average complimentary sensitivity
    sens_storage = sens_storage/resamples

    # Get total sensitivity index
    #sens_storage = 1.0 - sens_storage

    # Write TEI to file
    np.savetxt('EFAST_Total_Effect_Indices.csv',np.hstack((xx,sens_storage)),delimiter=',',header=header,fmt='%10.15f')
    

    os.chdir(wrkdir)

    return err
    


def sampled_scatter(xmin,xmax,Nsteps,lda,xtrain,props,instdev,disttype,paramlist='No Header'):
    # Scatter plot data for sampled distributions versus model output
    err  = "sampled_scatter: Clear"
    wrkdir = os.getcwd()
    os.mkdir("Sampled_Scatter_Plot")
    os.chdir("Sampled_Scatter_Plot")
    simpoints = 10000
    samples_generated = 0
    num_rejected = 0
    M = np.zeros((simpoints,len(props)),dtype=float)
    progress_bar(0,simpoints,'Parameter Scatter')
    for j in range(0,simpoints):
        progress_bar(j,simpoints,'Parameter Scatter')
        while True:
            # generate samples until one is not rejected
            rand_props = randprops(props,instdev,disttype)
            xx, yy, reject = mdl.model(xmin,xmax,Nsteps,rand_props,lda,xtrain)
            rand_props = rand_props.reshape(1,len(rand_props))
            M[j,:] = rand_props
            samples_generated += 1
            #print reject
            if any(reject) == True:
                num_rejected += 1
            else:
                break
    param_min = np.zeros(M.shape[1],dtype=float)
    param_max = np.zeros(M.shape[1],dtype=float)
    param_mean = np.zeros(M.shape[1],dtype=float)
    param_stdev = np.zeros(M.shape[1],dtype=float)
    f = open('Parameter_Statistics.csv','w')
    f.write('#Parameter,Mean,Min,Max,Stdev\n')
    # get max, min, mean for each sampled parameter
    for i in range(0,len(props)):
        param_min[i] = np.nanmin(M[:,i])
        param_max[i] = np.nanmax(M[:,i])
        param_mean[i] = np.nanmean(M[:,i])
        param_stdev[i] = np.nanvar(M[:,i]**0.5)
        f.write(paramlist[i]+','+str(param_mean[i])+','+str(param_min[i])+','+str(param_max[i])+','+str(param_stdev[i])+'\n')
    f.close()
    progress_bar(1,1,'Parameter Scatter')
    print "Sample rejection rate: %s" %(float(num_rejected/samples_generated))

    np.savetxt('Parameter_samples.csv',M,delimiter=',',header=','.join(paramlist))
    progress_bar(0,1,'Translate Histogram')
    for j in range(0,len(props)):
        progress_bar(j,len(props))
        if instdev[j] == 0:
            continue
        binvals,normhist = fasthist(M[:,j],paramlist[j])
        mp.modplot((binvals,normhist),(''),('k-'),"%s_histogram" %paramlist[j],paramlist[j],"Normalized Frequency",'False','False','False')
    progress_bar(1,1,'Translate Histogram')
    os.chdir(wrkdir)
    return err

if __name__ == '__main__':
    print "My future is so bright, I'm scientifically obligated to wear shades. "


#!/usr/bin/python
'''

Copyright (c) 2016 Justin M. Hughes

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

import numpy as np

'''
Radial Basis Function (RBF) script for training RBF model
	Handles a single test point (for MC sampling)
	For MC sampling, pass lda matrix to mc_rbf

x - training points: [num_points by num_vars] (numpy.array)
y - vector of function values for each x [by row] (numpy.array)
x_test - test point for predicting with RBF (numpy.array)
c - RBF constant
rbf_type - radial basis function to be used
		1 - Thin plate
		2 - Gaussian
		3 - Multiquadric
		4 - Inverse Multiquadric
'''

def get_norm(vec):
	# Returns the 2-norm (Euclidean distance) of vec
	a = 0
	for i in range(0,len(vec)):
		a += vec[i]**2.0
	if a == 0:
		return 0
	else:
		return np.sqrt(a)

def get_phi(r,c,rbf_type):
	if rbf_type == 1:
		if r==0:
			return 0
		else:
			return np.log(c*r)*r*r	
	elif rbf_type == 2:
		return np.exp(-1.0*c*r*r)
	elif rbf_type == 3 or rbf_type == 4:
		phi = np.sqrt(r*r + c*c)
		if rbf_type == 4:
			phi = 1.0/phi
		return phi

def norm_params(vec):
# 06/20/2016 Testing hypercube normalization
# All parameters range from 0 to 1
    normvec = vec-vec
    for j in range(0,normvec.shape[1]):
        maxval = max(vec[:,j])
        minval = min(vec[:,j])
        #print minval, maxval
        normvec[:,j] = (vec[:,j]-minval)/(maxval - minval)
        #print normvec[:,j]
    return normvec

def lda_matrix(x,y,c,rbf_type):
    # Get number of training points and parameters
    n = x.shape[0]
    y = y.T
    
    #print minvals, maxvals

    #Set up phi storage variable
    A = np.zeros((n,n))

    # Normalize each dimension to range from 0 to 1
    x = norm_params(x)
    #print x

    # Debugging normalizing
    #print x

    # Get phi matrix (radii)
    for i in range(0,n):
        for j in range(0,n):
            r = get_norm(x[j,:]-x[i,:]) 
            A[i,j] = get_phi(r,c,rbf_type)
    
    #np.savetxt('UQSA_phi.csv',A,delimiter=',')
    
    lda = np.zeros((y.shape[0],y.shape[1]))
	
    # Solve for lambda
    for i in range(0,len(y[0,:])):
        try:
            lda[:,i] = np.dot(np.linalg.inv(A),y[:,i])
        except:
            #print "Phi matrix is singular, using pseudoinverse..."
            lda[:,i] = np.dot(np.linalg.pinv(A),y[:,i])
    return lda
	
if __name__ == "__main__":
	print "This script contains:"
	print "\typred_rbf(x,y,x_test,c,rbf_type) - RBF prediction"
	print "\tget_phi(r,c,rbf_type) - basis function selection and solution"
	print "\tget_norm(vec) - Euclidean distance of vector"


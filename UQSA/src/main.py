# 487339606 - 5a
'''

Copyright (c) 2016 Justin M. Hughes

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''
import subprocess
import numpy as np
import os,sys,warnings,atexit,math,datetime
from time import clock,strftime
from histgen import histgen_multi
import sensitivity_analysis as sa
import unc_propagation as ppn
import unc_monte_carlo as umc
import modplot as mp

def mc_with_local(xxmin,xxmax,simpoints,num_div,lda,xtrain,props,stdevs,dist,parameter_labels,xlabel,ylabel,logx,logy,swapxy,perform_mc='False',local_sa='False'):
    err = []    
    modstdev = []
    if perform_mc == 'True':
        print "Performing Monte Carlo simulations..."
        modstdev, mcerr = umc.monte_carlo_srs(xxmin,xxmax,simpoints,num_div,lda,xtrain,props,stdevs,dist,parameter_labels,xlabel,ylabel,logx,logy,swapxy)
        err.append(mcerr)
    
    if local_sa == 'True':
        print "Performing Local Sensitivity Analysis..."
        err.append(sa.sens_local_normed(xxmin,xxmax,num_div,lda,xtrain,props,stdevs, \
                                        modstdev,parameter_labels,'',perform_mc))

    return err

def mca_main(parameter_labels,srcdir,lda=0,xtrain=0):
    err = []
    os.chdir(srcdir)
	
    # Recover setup file, start analysis
    suf = open('mcsetup.dat','r')
    suflines = suf.read()
    suflines = suflines.split('\n')
    suf.close()
	
    # Get setup information from mcsetup.dat
    wrkdir = str(suflines[0])
    infile = str(suflines[1])
    perform_propagation = suflines[3]
    perform_mc = suflines[4]
    local_sa = suflines[5]
    tei_sa = suflines[6]
    vratio_sa = suflines[7]
    efast_sa = suflines[8]
    fast_sa = suflines[9]
    scatter_sa = suflines[10]
    xxmin = float(suflines[14])
    xxmax = float(suflines[15])
    num_div = int(suflines[17])
    simpoints = int(suflines[18])
    indvar = str(suflines[19])
    samples = int(suflines[20])
    resamples = int(suflines[21])
    freq_type = str(suflines[22])
    shuffle = suflines[23]
    xlabel = suflines[24]
    ylabel = suflines[25]
    swapxy = suflines[26]
    logx = suflines[27]
    logy = suflines[28]    
    
    # Get user defined model
    import user_model as mdl
    os.chdir(wrkdir)
    
    #This avoids a long list of ValueError warnings. Happens mostly with
    #   equations rasied to a power (negative to non-integer power) generally output as 'nan's
    warnings.filterwarnings("ignore")

    inputs = np.genfromtxt(infile,delimiter=',',usecols=(1,2,3),skip_header=1)
    props = inputs[:,0]
    stdevs = inputs[:,1]
    dist = inputs[:,2]
    
    # Get calibrated model output
    print "Evaluating for base parameter set..."
    xx,mod,reject = mdl.model(xxmin,xxmax,num_div,props,lda,xtrain)
    # Debugging    
    #return 0
    np.savetxt('MODEL_RUN.csv',np.hstack((xx,mod)),delimiter=',',header="X,Model Output",fmt='%15.10f')
    print "Plotting model output..."  
    err.append(mp.modplot((xx,mod),("Model"),("k-"),"Model",xlabel,ylabel,logx,logy,swapxy))
    
    # Call MC and local SA (local_normed requires output from MC)
    if perform_mc == 'True' or local_sa == 'True':
        err.append(mc_with_local(xxmin,xxmax,simpoints,num_div,lda,xtrain,props,stdevs,dist,parameter_labels,xlabel,ylabel,logx,logy,swapxy,perform_mc,local_sa))

    # Call TSE routine
    if perform_propagation == 'True' or local_sa == 'True':
        print "Performing General Uncertainty Analysis..."
        err.append(ppn.steele_prop(xxmin,xxmax,num_div,lda,xtrain,props,stdevs,parameter_labels,xlabel,ylabel,logx,logy,swapxy))

    # Call sensitivity analysis routines
    if tei_sa == 'True':
        print "Calculating Total Effect Indices (Global Sensitivity Analysis)..."
        err.append(sa.sens_global_EFAST(xxmin,xxmax,num_div,lda,xtrain,props,stdevs, \
                                       dist,samples,resamples,freq_type,shuffle,parameter_labels))

    if fast_sa == 'True':
        print "Performing Fourier Amplitude Sensitivity Test..."
        err.append(sa.sens_global_FAST(xxmin,xxmax,num_div,lda,xtrain,props,stdevs, \
                                       dist,samples,resamples,freq_type,shuffle,parameter_labels))

    if vratio_sa == 'True':
        print "Calculating First Order Effect Indices via Monte Carlo..."
        err.append(sa.sens_global_vratio(xxmin,xxmax,num_div,lda,xtrain,simpoints,props,stdevs,dist,indvar,parameter_labels))

    if scatter_sa == 'True':
        print "Getting scatter plot data for model inputs and evaluations..."
        err.append(sa.sampled_scatter(xxmin,xxmax,num_div,lda,xtrain,props,stdevs, \
                                      dist,parameter_labels))
    #Display a summary of results from simulations
#    err.append(modplot.plot(xx,mod,modmean,modmax,modmin, \
#                            modupperconfidence,modlowerconfidence, \
#                            '',plot_switches))
    
    os.chdir(srcdir)
    
    for e in err:
        print e
    print "\n\nAnalysis Complete."
    return err

if __name__ == "__main__":
    print "One of these days, Ndnd. Bang! Zoom! - Straight to the third moon of Omicron Persei 8!"

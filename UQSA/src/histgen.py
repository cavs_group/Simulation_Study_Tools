'''

Copyright (c) 2016 Justin M. Hughes, William R. Williams

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

import os
import numpy as np
import scipy as sp
import math
import modplot as mp
from terminal_progress_bar import progress_bar

def truncate_to_dec(number,decimals):
        number = number*(10**decimals)
        number = int(number)
        return float(number)/(10**decimals)
    
def getsub(vector,start,end):
    vec_length = abs(end - start)
    zeroes = [0] * vec_length
    for i in range(0,vec_length):
        zeroes[i] = vector[start+i]
    return np.array(zeroes)

def histgen_multi(data,maximums,minimums,means,xdata,name,parameter_labels='',logx=False,axis_label="X"):            #defining function
        '''
            HISTOGRAM GENERATION FUNCTION FOR MODEL OUTPUT
            Current version is for model ouput on multiple levels
        '''
        levels = len(maximums)                                  #Length of Maximums list, aka number of strain levels
        maxbins = 0
        shift = np.zeros(levels,dtype=float)                                #Creating one dimension array filled with zeros for each strain level
        observations = np.zeros(levels,dtype=float)                         #Creating one dimension array filled with zeros for each strain level
        iqr = np.zeros(levels,dtype=float)
        bin_size = np.zeros(levels,dtype=float)
        p25 = np.zeros(levels,dtype=float)
        p975 = np.zeros(levels,dtype=float)
        median = np.zeros(levels,dtype=float)
        maxbins = 0
        excluded = []
        # Output descriptive statistics to file for each dataset
        myfile = open('MC_Uncertainty.csv','w')
        myfile.write("X, Mean, Min, Max, Bin Size,Inner Quartile Range,Median (50%),2.5%,97.5\n")
        for i in range(0,levels):
                p25[i] = np.nanpercentile(data[i,:],2.5)
                p975[i] = np.nanpercentile(data[i,:],97.5)
                iqr[i] = np.subtract(*np.nanpercentile(data[i,:],[75,25]))
                median[i] = np.nanpercentile(data[i,:],50)
                if iqr[i] == 0 or str(iqr[i]) == 'nan':
                    excluded.append((i))
                    continue
                bin_size[i] = abs(truncate_to_dec((2*iqr[i]/(len(data[0,:])**(1.0/2.0))),12))
                if bin_size[i] == 0 or str(bin_size[i]) == 'nan':
                    excluded.append((i))
                    continue
                print "Level: %s, Bin size: %s, IQR: %s" %(i, bin_size[i], iqr[i])
                #myfile.write(str(i)+','+str(means[i])+','+str(minimums[i])+','+str(maximums[i])+','+str(bin_size[i])+','+str(iqr[i])+','+str(p25)+','+str(p975)+'\n')
                shift[i] = int(minimums[i]/bin_size[i])
                if (maximums[i] - minimums[i])/bin_size[i] > maxbins:
                        maxbins = int((maximums[i] - minimums[i])/bin_size[i])+1
        
        hist =  np.zeros((maxbins,levels))                      #Two dimensional array declared filled with zeros, row=maxbin column=levels
        print "\t %s Histogram bins" % maxbins
        
       
        for i in range(0,levels):                               #i is levels, running through each strain level
                observations[i] = 0
                if iqr[i] == 0 or bin_size[i] == 0 or str(iqr[i]) == 'nan':
                    myfile.write(str(float(xdata[i]))+',nan,nan,nan,nan,nan,nan,nan,nan\n')
                    continue
                #print bin_size[i],shift[i]
                for j in range(0,len(data[i,:])):               #j is data point, declared on i'th level. J counter goes from 0 to length of datapoints on lvl
                        if math.isnan(data[i,j]) == False:
                                index = int(data[i,j]/(bin_size[i]) - shift[i] )       #Taking datapoint, shifting magnitude, subtracting shifted min to get distance from min, converting to int
                                if index < maxbins - 1:                                    #If distance to min is smaller than maxbin, or is zero
                                        hist[index,i] = hist[index,i] + 1               #Increment histogram
                                        observations[i] += 1                            #Count the sheep on that level
                myfile.write(str(float(xdata[i]))+','+str(float(means[i]))+','+str(float(minimums[i]))+','+str(float(maximums[i]))+','+str(bin_size[i])+','+str(iqr[i])+','+str(median[i])+','+str(p25[i])+','+str(p975[i])+'\n')
        myfile.write('\n'+"Histogram Bins,"+str(maxbins)+'\n')
        myfile.close()
       
        # To speed up plot output, only the first, middle, and last level will be plotted
        #final_index = levels-1                                                  #10 strain levels will be indexed 0 to 9.. therefore index is levels-1
        #index_list = [0, int(final_index/2),final_index]                        #Array of first, middle, and last strain levels
        indices = np.arange(0,len(hist[:,0]),1)                                 #Array from 0 to length of 1st strain level in increments of 1
        bin_vals = np.zeros((levels,len(indices)))

        #Recovering bin left edge values
        for i in range(0, levels):
                for j in range(0,indices[-1]):
                        bin_vals[i,j] = bin_size[i]*(indices[j]+shift[i])
      
        bin_size = bin_size.reshape(1,levels)                                         #Changing power array to a single row with column of each level
        shift = shift.reshape(1,levels)                                         #Changing shifting minINC array to a single row with column of each level
        observations = observations.reshape(1,levels)                           #Changing sheep counting array to a single row with column of each level
        
        #Translate histograms to better format
        histout = np.vstack((bin_size,shift,observations,hist))
        try:
            translate_hist_multi(histout,excluded,parameter_labels,logx)
        except ValueError:
            print "Error in histogram translator"
        return histout

def translate_hist_multi(histdata,excluded,parameter_labels,logx,filename='hist'):
    print "\nTranslating Histograms..."
    start_directory = os.getcwd()
    
    #Get number of histograms in histdata
    foldername = "Histograms"
    os.mkdir(foldername)
    os.chdir(foldername)
    histdir = os.getcwd()
    os.mkdir('PLOTS')
    numsets = len(histdata[0,:])
    #progress_bar(0,numsets,'Translate Histogram')
    #Create and move to new working directory
    for i in range(0,numsets):
        excluded_flag = False
        for x in excluded:
            if i == x:
                excluded_flag = True
        if excluded_flag == True:
            #print "Excluded"
            continue
        
        #print ("Translating histogram: %s" %(i))
        progress_bar(i,numsets,'Translate Histogram')
        current = histdata[:,i]
        writename = str(i)+"_"+filename
            
        # get histogram properties
        bin_size = current[0]
        shift = current[1]
        observations = current[2]
        hist = getsub(current,3,len(current)-1)

        normhist = [float(entry)*abs(bin_size) for entry in hist]

        area = sum(normhist)
        normhist = hist/area
            
        binnums = range(0,len(hist))
                
        #Bin Center values
        binvals = (binnums+shift)*bin_size + bin_size/2.0

        os.chdir('PLOTS')
        mp.modplot((binvals,normhist),(''),('k-'),writename,'X','Normalized Frequency',logx,'False','False')
        os.chdir(histdir)
        
        # Stack arrays for file write
        #print bin_size, shift, observations
        #print len(binvals), len(hist)
        binvals = binvals.reshape(len(binvals),1)
        hist = hist.reshape(len(hist),1)
        normhist = normhist.reshape(len(normhist),1)

        # Traded normhist for pdf (assign k/n probability for each bin)
        #pdf = hist/observations
        cumulative = np.cumsum(hist/observations)
        cumulative = cumulative.reshape(len(cumulative),1)
        tofile = np.hstack((binvals,hist,normhist,cumulative))
    
        #Push formatted histograms to unique files
        np.savetxt(writename+".csv",tofile,delimiter=',',header="Center Bin Values, Histogram, Normalized,CDF,Obs=%s,Shift=%s,Bin Size=%s,Area=%s" %(observations, shift,bin_size,area),fmt='%15.24f')
    progress_bar(1,1,'Translate Histogram')
    os.chdir(start_directory)

def fasthist(data,name='none'):
    # histogram generation for a single vector of data
    minimum = np.nanmin(data)
    maximum = np.nanmax(data)
    mean =  np.nanmean(data)
    p25 = np.nanpercentile(data,2.5)
    p975 = np.nanpercentile(data,97.5)
    iqr = np.subtract(*np.nanpercentile(data,[75,25]))
    if iqr == 0:
        return 0
    bin_size = abs(truncate_to_dec((2*iqr/(len(data)**(0.5))),12))
    shift = int(minimum/bin_size)
    bins = int((maximum-minimum)/bin_size) + 2
    hist = np.zeros(bins,dtype=float)
    print name," Bin size: %s IQR: %s" %(bin_size,iqr)
    for entry in data:
        if str(entry) != 'nan':
            index = int(entry/bin_size-shift)
            hist[index] += 1
    normhist = [float(entry)*abs(bin_size) for entry in hist]
    area = sum(normhist)
    normhist = hist/area
    binnums = range(0,len(hist))
    binvals = [(num+shift)*bin_size + bin_size/2.0 for num in binnums]

    return binvals, normhist

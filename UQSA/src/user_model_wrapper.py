#!/usr/bin/python
'''

Copyright (c) 2016 Justin M. Hughes

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''
import os

def wrapper(model_name,paramfile,srcdir,indvar='x'):
    startdir = os.getcwd()
    os.chdir(srcdir)
    print "Meshing parameter file and model file"
    wrapper_error = "clear"
    extensions = ('','.txt','.py','.csv')
        
    if os.path.isfile("user_model.py") == True:
        os.remove("user_model.py")

    if os.path.isfile("user_model.pyc") == True:
        os.remove("user_model.pyc")

    param_labels = []
    mod = open(model_name,'r')
    pf = open(paramfile,'r')
    pflines = pf.readlines()
    linestr = [str(cl) for cl in pflines]
    for entry in linestr:
        if not entry.startswith('#'):
            param_labels.append(entry.split(',')[0])
            
    # Get user input code
    modlines = mod.readlines()
    sublines = []
    mathlines = []
    surrogate = False
    # Identify and grab user defined subroutines
    parsing = False
    for line in modlines:
        if line.startswith("#__startsub__"):
            parsing = True
            continue
        elif line.startswith("#__endsub__"):
            parsing = False
        if parsing:
            sublines.append(line)
            
    # Grab the rest of the lines
    parsing = False
    for line in modlines:
        if line.startswith("#__startmodel__"):
            parsing = True
            continue
        if line.startswith("#__SURROGATE__"):
            surrogate = True
            continue	
        if parsing:
            mathlines.append(line)
    
    # Write new code file
    writefile = open('user_model.py','w')
    writefile.write(
        "#!/usr/bin/python\n\n"
        "import numpy as np\n"
        "#Grab common math functions\n"
        "from numpy import sin,cos,tan,arcsin,arccos,arctan,hypot,arctan\n"
        "from numpy import arctan2,degrees,radians,unwrap,deg2rad,rad2deg\n"
        "from numpy import sinh,cosh,tanh,arcsinh,arccosh,arctanh\n"
        "from numpy import around,round_,rint,fix,floor,ceil,trunc\n"
        "from numpy import exp,log,log10,matrix,zeros\n")
    
    param_count = 0
    writefile.write("\n#USER DEFINED FUNCTIONS\n")
    for line in sublines:
        writefile.write(line)

# Changed model call. Will now "draw lines" versus generate data
#   at discrete x value. Avoids problems for models with discrete
#	integration schemes (like ISV models).
    if surrogate:
        mathlines = ['\t' + line for line in mathlines]
        writefile.write("\ndef model(xxmin,xxmax,Nsteps,props,lda=0,xtrain=0):\n")
        for label in param_labels:
            writefile.write('\t'+label + ' = props[%s]\n' %(str(param_count)))
            param_count += 1  
        writefile.write("\t#Default rejection value\n")
        writefile.write("\treject = False\n")
        for line in mathlines:
            writefile.write(line)
        writefile.write('\t#Get surrogate prediction\n')
        writefile.write('\tyy = output\n')
        writefile.write('\txx = np.array(xx).reshape(len(xx),1)\n')
        writefile.write('\tyy = np.array(yy).reshape(len(yy),1)\n')
        writefile.write('\n\n\treturn xx,yy,rr')
    else:
        # tab model lines for wrapper
        mathlines = ['\t\t'+line for line in mathlines]
        writefile.write("\ndef model(xxmin,xxmax,Nsteps,props,lda=0,xtrain=0):\n")
        writefile.write("\t#Default rejection value\n")
        writefile.write("\treject = False\n")
        writefile.write("\n\t#Set dx\n")
        writefile.write("\tdxx = float((xxmax - xxmin)/Nsteps)\n")
        writefile.write("\txx = zeros([Nsteps+1,1],dtype=float)\n")
        writefile.write("\tyy = zeros([Nsteps+1,1],dtype=float)\n")
        writefile.write("\trr = [False]*(Nsteps+1)\n")
        writefile.write("\tfor i in range(0,Nsteps+1):\n")
        writefile.write("\t\txx[i] = xxmin +i*dxx\n")
        writefile.write("\n\t#Store parameters in easy to read format\n")
        for label in param_labels:
            writefile.write('\t'+label + ' = props[%s]\n' %(str(param_count)))
            param_count += 1
        # set up model evaluation loop
        writefile.write("\tfor i in range(0,Nsteps+1):\n"
                        "\t\tx = xx[i]\n")
        writefile.write("\n\t\t# Set independent variable to x!\n")
        writefile.write("\t\t%s = x\n" % indvar)
        for line in mathlines:
            writefile.write(line)
        writefile.write("\n\t\t# Write output variable to analysis variable\n")
        writefile.write("\t\tyy[i] = output\n")
        writefile.write("\t\trr[i] = reject\n")
        writefile.write("\n\n\treturn xx,yy,rr")
    
        writefile.close()
    mod.close()
    os.chdir(startdir)
    return wrapper_error,param_labels

if __name__ == '__main__':
    cwd = os.getcwd()
    err,labels = wrapper('model_default.txt','params_default.csv',cwd)

    xmin = 0.0
    xmax = 10.0
    Nsteps = 100
    import numpy as np
    import matplotlib.pyplot as plt
    props = np.genfromtxt('params_default.csv',delimiter=',',usecols=1)
    
    import user_model as mdl

    xx, yy = mdl.model(xmin,xmax,Nsteps,props)

    plt.plot(xx,yy,'k-',linewidth=2)
    plt.show()
    
    

#!/usr/bin/python
'''

Copyright (c) 2016 Justin M. Hughes

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''
import matplotlib.pyplot as plt
import matplotlib.ticker as tkr

def modplot(plot_data,descriptor,style,plotname,xlabel,ylabel,logx,logy,swapxy):
    # plot_data: list of vectors containing the xy datasets, sequentially
    #            (x1,y1,x2,y2,....)
    # descritpors: list of lengend titles
    # axislables: labels for x and y axes
    # settings: plot settings (swapxy, logx, logy)
    err = 'Modplot: Clear'
    
    num_sets = len(plot_data)/2

    if num_sets > 1:
        try:
            #print "Modplot: plotting %s data sets..." %(num_sets)
            #plt.title(plotname,fontsize=24)
            fig = plt.figure()
            ax = fig.add_subplot(1,1,1)
            
            if logx == 'True':
                ax.set_xscale('log')
            if logy == 'True':
                ax.set_yscale('log')
            setcount = 0
            for i in range(0,num_sets):
                xdata = plot_data[setcount]
                ydata = plot_data[setcount+1]
                #ax.set_xlim([0.95*min(xdata),1.05*max(xdata)])
                #ax.set_ylim([0.95*min(ydata),1.05*max(ydata)])
                if swapxy == 'False':
                    plt.ylabel(r'%s' %ylabel,fontsize=18)
                    plt.xlabel(r'%s' %xlabel,fontsize=18)
                    ax.plot(xdata,ydata,style[i],linewidth=2,label=descriptor[i])
                else:
                    plt.xlabel(r'%s' %ylabel,fontsize=18)
                    plt.ylabel(r'%s' %xlabel,fontsize=18)
                    ax.plot(ydata,xdata,style[i],linewidth=2,label=descriptor[i])
                
                setcount+=2
    
            plt.subplots_adjust(hspace=0.5)
            plt.legend(loc='best')
            plt.savefig(plotname+"_MODPLOT")
            plt.clf()
        except:
            print "Modplot: Failed"
            err = "Modplot: Failed"
    else:
        try:
            #print "Modplot: plotting 1 data set..."
            fig = plt.figure()
            ax = fig.add_subplot(1,1,1)
            if logx == 'True':
                ax.set_xscale('log')
            if logy == 'True':
                ax.set_yscale('log')
            xdata = plot_data[0]
            ydata = plot_data[1]
            #ax.set_xlim([0.95*min(xdata),1.05*max(xdata)])
            #ax.set_ylim([0.95*min(ydata),1.05*max(ydata)])
            if swapxy == 'False':
                plt.ylabel(r'%s' %ylabel,fontsize=18)
                plt.xlabel(r'%s' %xlabel,fontsize=18)
                ax.plot(xdata,ydata,style,linewidth=2,label=descriptor)

            else:
                plt.xlabel(r'%s' %ylabel,fontsize=18)
                plt.ylabel(r'%s' %xlabel,fontsize=18)
                ax.plot(ydata,xdata,style,linewidth=2,label=descriptor)

            plt.subplots_adjust(hspace=0.5)
            plt.legend(loc='best')
            plt.savefig(plotname+'_MODPLOT')
            plt.clf()
        except:
            print "Modplot: Failed"
            err = "Modplot: Failed"
            
    return err

if __name__ == "__main__":
    xx = range(0,10)
    yy = [0.5*i**2.0 for i in xx]
    data = (xx,yy)
    err = modplot(data,("Model1"),('k-'),"MonteCarlo",'X','Y','False','False','False')
    print err
    

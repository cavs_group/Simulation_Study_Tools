'''

Copyright (c) 2016 Justin M. Hughes

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''
import numpy as np
from terminal_progress_bar import progress_bar
import os,sys,warnings,atexit,math,datetime
from time import clock,strftime
from histgen import histgen_multi
import modplot as mp
import user_model as mdl

# Import rejection method for parameter sampling if available
try:
    from user_model import rejection_method
    user_rejection_method = True
except:
    user_rejection_method = False

def confidence_interval(data,confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.nanmean(a), stats.sem(a)
    h = se*abs(stats.t.ppf((1+confidence)/2.,n-1))
    return m, m-h, m+h

def statistics(data,lower=2.5,upper=97.5):
    a = 1.0*np.array(data)
    m = np.nanmean(a)
    lm = np.nanmean(np.log(a))
    lvar = np.nanvar(np.log(a))
    var = np.nanvar(a)
    lowerbound = np.nanpercentile(a,lower)
    upperbound = np.nanpercentile(a,upper)
    median = np.nanpercentile(a,50)
    return m,var,lowerbound,upperbound,lm,lvar,median

def randprops(props,stdevs,dist):
    rand = props-props
    for i in range(0,len(props)):
        if dist[i] == 0: # NORMAL DISTRIBUTION
            if stdevs[i] < 0:
                rand[i] = float(np.random.normal(-1.0*props[i],-1.0*stdevs[i],1))
                rand[i] = -1.0*rand[i]
            if stdevs[i] == 0:
                rand[i] = float(props[i])
            if stdevs[i] > 0:
                rand[i] = float(np.random.normal(props[i],stdevs[i],1))
        if dist[i] == 1: # UNIFORM DISTRIBUTION
            if stdevs[i] == 0:
                rand[i] = float(props[i])
            if stdevs[i] < 0:
                rand[i] = float(np.random.uniform(np.abs(props[i])-np.abs(stdevs[i]),np.abs(props[i])+np.abs(stdevs[i]),1))
                rand[i] = -1.0*rand[i]
            if stdevs[i] > 0:
                rand[i] = float(np.random.uniform(props[i]-stdevs[i],props[i]+stdevs[i],1))
        if dist[i] == 2: # LOGNORMAL DISTRIBUTION (CURRENTLY DOES NOT WORK, may have to create custom inverse lognormal function)
            if stdevs[i] < 0:
                rand[i] = float(np.random.lognormal(-1.0*props[i],-1.0*stdevs[i],1))
                rand[i] = -1.0*rand[i]
            if stdevs[i] == 0:
                rand[i] = float(props[i])
            if stdevs[i] > 0:
                rand[i] = float(np.random.lognormal(props[i],stdevs[i],1))
    return rand

def monte_carlo_srs(xxmin,xxmax,simpoints,num_div,lda,xtrain,props,stdevs,dist,parameter_labels,xlabel,ylabel,logx,logy,swapxy):
    err = 'monte_carlo_srs: clear'    
    wrkdir = os.getcwd()    
    os.mkdir("Monte_Carlo")
    os.chdir("Monte_Carlo")

    xx = np.zeros([num_div+1,1],dtype=float)
    modout = np.zeros([num_div+1,simpoints],dtype=float)
    modmean = np.zeros([num_div+1,1],dtype=float)
    modmin = np.zeros([num_div+1,1],dtype=float)
    modmax = np.zeros([num_div+1,1],dtype=float)
    modlogmean = np.zeros([num_div+1,1],dtype=float)
    modvar = np.zeros([num_div+1,1],dtype=float)
    modstdev = np.zeros([num_div+1,1],dtype=float)
    modmedian = np.zeros([num_div+1,1],dtype=float)
    modlogvar = np.zeros([num_div+1,1],dtype=float)
    modlowerconfidence = np.zeros([num_div+1,1],dtype=float)
    modupperconfidence = np.zeros([num_div+1,1],dtype=float)

    # Evaluate model for j simulations
    if user_rejection_method == False:
        print "Running Monte Carlo simulations. Please wait..."
    else:
        print "Running Monte Carlo simulation with user rejection method. Please wait..."
    
    progress_bar(0,simpoints,'MCSRS...')
    
    #reject = True
    samples_generated = 0
    num_rejected = 0
    for j in range(0,simpoints):
        while True:
            # generate samples until one is not rejected
            rand_props = randprops(props,stdevs,dist)
            xx, yy, reject = mdl.model(xxmin,xxmax,num_div,rand_props,lda,xtrain)
            samples_generated += 1
            #print reject

            if any(reject) == True:
                num_rejected += 1
            else:
                break
      
        #Inspect for NaNs, replace with local average
        num_factors = 5
        for i in range(0,len(yy)):
            if str(yy[i]) == 'nan':
                yy[i] = np.nanmean(yy[i-num_factors:i+num_factors])
        
        modout[:,j] = yy.T
        progress_bar(j,simpoints,'MCSRS...')
    progress_bar(simpoints,simpoints,'MCSRS...')

    if user_rejection_method == True:
        #print num_rejected, samples_generated
        ratio = float(num_rejected/samples_generated)
        print "Sample rejection rate: %s" %(ratio)

    print "Calculating Averages, Maximums, and Minimums..."
    for i in range(0,num_div+1):
        modmean[i], modvar[i], modlowerconfidence[i], modupperconfidence[i], modlogmean[i],modlogvar[i],modmedian[i] = statistics(modout[i,:],2.5,97.5)
        modmin[i] = np.nanmin(modout[i,:])
        modmax[i] = np.nanmax(modout[i,:])

    mp.modplot((xx,modmean,xx,modmedian,xx,modmin,xx,modmax,xx,modlowerconfidence,xx,modupperconfidence),('Mean','Median','Min','Max','2.5%','97.5%'),('k-','g--','b-','b-','r--','r--'),"MC_Results",xlabel,ylabel,logx,logy,swapxy)
                    
    modstdev = np.array([x**0.5 for x in modvar])
    print "Writing Simulation Data to file..."
    np.savetxt('MC_rawdata.csv',np.vstack((xx.T,modmean.T,modmin.T,modmax.T,modout.T)),delimiter=',',header = "1 X, 2 Mean, 3 Min, 4 Max, 5 Data",fmt='%15.10f')
    np.savetxt('MC_Statistics.csv',np.hstack((xx,modmean,modvar,modmin,modmax,modlogmean,modlogvar)),delimiter=',',header="X,Mean,Var,Min,Max,LogMean,LogVar",fmt='%15.10f')
    print "Analyzing simulation data, generating histograms..."
    print "\n\t-- Model --"
    try:
        modhists = histgen_multi(modout,modmax,modmin,modmean,xx,'MC',parameter_labels,logx)
    except:
        err = 'monte_carlo_srs: Histogram generation failed.'

    print "\n"
    os.chdir(wrkdir)

    return modstdev, err


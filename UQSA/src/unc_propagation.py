'''

Copyright (c) 2016 Justin M. Hughes

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

import user_model as mdl
import modplot as mp
import numpy as np
import os

def perturb(i,props,pct=0.0001):
    pprops = np.empty(len(props),dtype=float)*0
    for j  in range(0,len(pprops)):
        pprops[j]= props[j]

    pprops[i] = pprops[i]*(1+pct)
    return pprops

def steele_prop(xmin,xmax,Nsteps,lda,xtrain,props,stdevs,paramlist,xlabel,ylabel,logx,logy,swapxy):
    
    err = 'steele_prop: Clear'
    paramlist = 'X,' + ','.join(paramlist)
    num_params = len(props)
    work_dir = os.getcwd()
    os.mkdir('Coleman_Steele_Propagation')
    os.chdir('Coleman_Steele_Propagation')
    local_sensitivity = np.zeros([Nsteps+1,num_params],dtype=float)
    uncert = np.zeros([Nsteps+1,num_params],dtype=float)
    UU = np.zeros([Nsteps+1,1],dtype=float)
    
    pct = 0.0001
    xx, yy, reject = mdl.model(xmin,xmax,Nsteps,props,lda,xtrain)
    
    # Calculate derivatives
    for j in range(0,num_params):
        xx, yu, reject = mdl.model(xmin,xmax,Nsteps,perturb(j,props,pct),lda,xtrain)
        xx, yd, reject = mdl.model(xmin,xmax,Nsteps,perturb(j,props,-1.0*pct),lda,xtrain)
        denom = 2*pct*props[j]
        local_sensitivity[:,j] = abs((yu.T - yd.T)/denom)
    
    # Calculate uncertainty matrix
    for j in range(0,num_params):
        uncert[:,j] = (local_sensitivity[:,j]**2.0)*(stdevs[j]**2.0)

    for i in range(0,Nsteps+1):
        UU[i] = sum(uncert[i,:])**0.5

    outdata = np.hstack((xx,yy,yy+UU,yy-UU,UU))
    np.savetxt("Uncertainty.csv",outdata,delimiter=',',header='X,Y,Upper,Lower,Uncertainty')
    outdata = np.hstack((xx,local_sensitivity))
    np.savetxt("Local_Sensitivity.csv",outdata,delimiter=',',header=paramlist)
    
    mp.modplot((xx,yy,xx,yy+UU,xx,yy-UU),('Model','Bounds',''),('k-','r-','r-'),'UNC_',xlabel,ylabel,logx,logy,swapxy)
    os.chdir(work_dir)
    
    return err

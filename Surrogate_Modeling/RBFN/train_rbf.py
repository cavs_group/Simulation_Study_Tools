import numpy as np
import matplotlib.pyplot as plt
import sys, math, os

import warnings
warnings.filterwarnings("ignore")

def get_norm(vec):
	# Returns the 2-norm (Euclidean distance) of vec
	a = 0
	for i in range(0,len(vec)):
		a += vec[i]**2.0
	if a == 0:
		return 0
	else:
		return np.sqrt(a)

def get_phi(r,c,rbf_type):
	if rbf_type == 1:
		if r==0:
			return 0
		else:
			return np.log(c*r)*r*r	
	elif rbf_type == 2:
		return np.exp(-1.0*c*r*r)
	elif rbf_type == 3 or rbf_type == 4:
		phi = np.sqrt(r*r + c*c)
		if rbf_type == 4:
			phi = 1.0/phi
		return phi

def norm_params(vec,test='none'):
	normvec = vec-vec
	#print normvec.shape
	if type(test) != type(vec):
		for j in range(0,normvec.shape[1]):
			maxval = max(vec[:,j])
			minval = min(vec[:,j])
			normvec[:,j] = (vec[:,j]-minval)/(maxval-minval)
		return normvec
	elif type(test) == type(vec):
		normtest = test-test
		#print normtest.shape
		for j in range(0,normvec.shape[1]):
			maxval = max(vec[:,j])
			minval = min(vec[:,j])
			normvec[:,j] = (vec[:,j]-minval)/(maxval-minval)
			normtest[:,j] = (test[:,j]-minval)/(maxval-minval)
		return normvec,normtest
        
def lda_matrix(x,y,c,rbf_type):
    # Get number of training points and parameters
    n = x.shape[0]
    y = y.T

    #Set up phi storage variable
    A = np.zeros((n,n))

    # Normalize each dimension to range from 0 to 1
    x = norm_params(x)
    #print x

    # Debugging normalizing
    #print x

    # Get phi matrix (radii)
    for i in range(0,n):
        for j in range(0,n):
            r = get_norm(x[j,:]-x[i,:]) 
            A[i,j] = get_phi(r,c,rbf_type)

    lda = np.zeros((y.shape[0],y.shape[1]))
    # Solve for lambda
    for i in range(0,len(y[0,:])):
        try:
            lda[:,i] = np.dot(np.linalg.inv(A),y[:,i])
        except:
            #print "Phi matrix is singular, using pseudoinverse..."
            lda[:,i] = np.dot(np.linalg.pinv(A),y[:,i])
    return lda, A
    
def ypred_rbf(xxmin,xxmax,Nsteps,x_test,x_train,y_train,c,rbf_type):

	dxx = float((xxmax-xxmin)/(Nsteps-1))
	xx = np.zeros([Nsteps,1],dtype=float)
	
	xx = [xxmin+i*dxx for i in range(0,Nsteps)]
	#Normalize each dimension by max expected value
	x_train,x_test = norm_params(x_train,x_test)
	#print x_train, x_test
	lda, A = lda_matrix(x_train,y_train,c,rbf_type)
    
	#print lda
	y_pred = np.zeros((lda.shape[1],1))
	#Get RBF prediction for test point at each x
	for j in range(0,len(lda[0,:])):
		#print len(y[0,:])
		for i in range(0,len(lda[:,0])):
			r = get_norm(x_test[0,:] - x_train[i,:])
			phi = get_phi(r,c,rbf_type)
			y_pred[j,0] += lda[i,j]*phi
			#print r, phi
	return y_pred,lda,A

def progress_bar(count,total,status=''):
    bar_len = 30
    filled_len = int(round(bar_len*count/float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '*' * filled_len+'-'*(bar_len-filled_len)
    sys.stdout.write('[%s] %s%s %s\r' %(bar,percents,'%',status))
    sys.stdout.flush()
    if count == total:
        sys.stdout.write('\n')
        sys.stdout.flush()

def get_data(filename,doe_points):
    data = np.genfromtxt(filename,delimiter=',',skip_header=1)
    xdata = np.array(data[:,0]).reshape(len(data[:,0]),1)
    ydata = np.array(data[:,1:-1])

    doe = np.genfromtxt(doe_points,delimiter=',',skip_header=1)
    sets = len(ydata[0,:])

    return xdata,ydata,len(ydata[0,:]),doe

def split(xdata,ydata,numsets,trainsets,points):

    training_data = ydata[:,0:trainsets]
    target_data = ydata[:,trainsets:-1]
    training_points = points[0:trainsets,:]
    target_points = points[trainsets:-1,:]
    
    return training_data,training_points,target_data,target_points

def get_rmse(test,true):
    diff = []
    for i in range(0,len(test)):
        diff.append((test[i]-true[i])/true[i])

    diff = [x**2.0 for x in diff]
    sumdiff = np.nansum(diff)
    
    return np.sqrt(sumdiff/len(diff))

def epoch(data,doe,trainsets,cmin,cmax,csteps,rbf_type):
    # Grab training data, training points
    xx,yy,sets,points = get_data(data,doe)
    crange = np.linspace(cmin,cmax,csteps)

    training_data,training_points,target_data,target_points = split(xx,yy,sets,trainsets,points)
    num_targets = len(target_points[:,0])
    #print(num_targets)
    Nsteps = target_data.shape[0]

    targeterr = np.zeros((len(crange),num_targets),dtype=float)
    avgtargeterr = np.zeros((len(crange)),dtype=float)

    for i in range(0,num_targets-1):
        x_test = target_points[i,:].reshape(1,target_points.shape[1])
        y_target = target_data[:,i]
        err = []
        for cval in crange:
            ypred,lda,A = ypred_rbf(0.0,1.0,Nsteps,x_test,training_points,training_data,cval,rbf_type)
            rmse = get_rmse(ypred,y_target)
            err.append(rmse)
        targeterr[:,i] = err

    # Compute average RMSE at each cval
    avgtargeterr = []
    for i in range(0,len(crange)):
        avgtargeterr.append(np.nanmean(targeterr[i,:]))

    #minerr = np.nanmin(avgtargeterr)
    #print(minerr)
    #cminerr = avgtargeterr.index(minerr)

    return crange,avgtargeterr

if __name__ == '__main__':

    data = str(sys.argv[1])
    doe = str(sys.argv[2])
    trainsets = int(sys.argv[3])
    cmin = float(sys.argv[4])
    cmax = float(sys.argv[5])
    csteps = int(sys.argv[6])
    
    rtype = range(1,5)

    logfile = open('RBFtrain.log','w')
    logfile.write("Working Dir: %s\n" %(os.getcwd()))
    logfile.write("Data file: %s\nPoints file: %s\n" %(data, doe))
    logfile.write("Cmin: %s\t Cmax: %s\t Steps: %s\n" %(cmin,cmax,csteps))
    for rbf_type in rtype:

        if rbf_type == 1:
            rbf_descriptor = "Thin Plate"
            rcolor = 'ko-'
        elif rbf_type == 2:
            rbf_descriptor = "Gaussian"
            rcolor = 'ro-'
        elif rbf_type == 3:
            rbf_descriptor = "Multiquadric"
            rcolor = 'go-'
        elif rbf_type == 4:
            rbf_descriptor = "Inverse Multiquadric"
            rcolor = 'bo-'

        print("\nRBF Type: %s" %rbf_descriptor)
        print("Sets\tMinErr\tCval")
        print("----\t------\t----")

        logfile.write("\nRBF Type: %s\n" %rbf_descriptor)
        logfile.write("Sets\tMinErr\tCval\n")
        logfile.write("----\t------\t----\n")

        trange = range(5,trainsets+1)
        terr = []
        for trainsets in trange:
            crange,err = epoch(data,doe,trainsets,cmin,cmax,csteps,rbf_type)
    
            #plt.plot(crange,err,linewidth=2,label="%s" %trainsets)
            minerr = np.nanmin(err)
            cminerr = crange[list(err).index(minerr)]
            terr.append(minerr)
            print("%s\t%s\t%s" %(trainsets,minerr,cminerr))
            logfile.write("%s\t%s\t%s\n" %(trainsets,minerr,cminerr))
            
        plt.plot(trange,terr,rcolor,linewidth=2,label=rbf_descriptor)
    plt.legend(loc='best')
    plt.savefig("MinErr_vs_Sets")
    logfile.close()


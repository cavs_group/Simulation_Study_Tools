import numpy as np
import matplotlib.pyplot as plt
import sys, math, os, random
from copy import deepcopy
import scipy.stats as st

import warnings
warnings.filterwarnings("ignore")

#####################################################################
          # Latin-Hypercube Sampling functions
#####################################################################

def getIndex(val,h,minVal=0.0,maxVal=1.0):
    minIndex = int(float(minVal/h))
    valIndex = int(float(val/h))-minIndex
    
    return valIndex

def indexToEdge(dim,lEdge,h,minVal=0.0,maxVal=1.0):

    rEdge = deepcopy(lEdge)

    for i in range(0,len(lEdge)):
        for j in range(0,dim):
            lEdge[i][j] = getFloatVal(lEdge[i][j],h)
            rEdge[i][j] = lEdge[i][j]+h
    
    return lEdge,rEdge

def getFloatVal(index,h,minVal=0.0,maxVal=1.0):
    return float(index*h+minVal)

def initializeSpace(dim,numStrata,minVal=0.0,maxVal=1.0,debug='False'):
    dimList = []
    for i in range(0,dim):
        dimList.append(numStrata)    

    h = float((maxVal-minVal)/(numStrata))

    return h,getIndex(maxVal,h)

def buildList(dim,numStrata):
    eligibleIndices = []
    iList = []

    for i in range(0,numStrata):
        iList.append(i)
    
    for i in range(0,dim):
        eligibleIndices.append(iList)    

    return eligibleIndices

def getLimitedDraw(dim,eligibleIndices,history):
    # set eligible indices for each dimension
    
    indices = deepcopy(eligibleIndices)    

    for ent in history:
        for i in range(0,dim):
            indices[i] = [x for x in indices[i] if x != ent[i]]

    point = []
    for i in range(0,dim):
        point.append(random.choice(indices[i]))

    return point, indices

def getDraw(dim,eligibleIndices,maxIndex,history):

    eIndex = deepcopy(eligibleIndices)

    #print(len(eIndex[0]))

    point = [0]*dim
    draws = 0
    rejected = 0
    invalid = []
    while True:
        if history == []:
            # Initial random draw
            point = [np.random.randint(0,maxIndex) for x in point]
            history.append(point)
            #print(len(history))
            break
        elif history != []:
            # Limited draw by limiting sample-able indices
            point,eIndex = getLimitedDraw(dim,eIndex,history)
            history.append(point)
            #print(len(history))
            break
                
            '''      
            #iterate over each index, compare to history
            for ent in history:
                for i in range(0,dim):
                    if point[i] == ent[i]:
                        invalid.append("True")
                    else:
                        invalid.append("False")

            if "True" in invalid:
                draws += 1
                rejected += 1
            else:
                draws += 1
                history.append(point)
                break
           '''

    return list(point),history,eIndex

def convertToRandomCDF(dim,history,h):
    #Get bin left edge
    leftEdge = deepcopy(history)
    rightEdge = deepcopy(history)

    #iterate over leftEdge, convert ints to floats
    
    leftEdge, rightEdge = indexToEdge(dim,leftEdge,h)

    randCDFVal = deepcopy(leftEdge)

    for i in range(0,len(leftEdge)):
        for j in range(0,dim):
            randCDFVal[i][j] = float(np.random.uniform(leftEdge[i][j],0.999999*rightEdge[i][j],1))
 
    return randCDFVal

def CDFtoNorm(CDF):
    dim = len(CDF[0])
    CDF = deepcopy(CDF)
    for i in range(0,len(CDF)):
        for j in range(0,dim):
            CDF[i][j] = st.norm.ppf(CDF[i][j])

    return CDF
        
def wtf(data,filename):
    f = open(filename,'w')
    for ent in data:
        for thing in ent:
            f.write(str(thing)+',')
        f.write('\n')
    f.close()

def sample(dim,numSamples,visualize='False'):
    # Generates standard uniform U(0,1) and standard normal N(0,1) CDF values
    numStrata = numSamples

    history = []

    h,maxIndex = initializeSpace(dim,numStrata)
    
    eIndices = buildList(dim,numStrata)

    for i in range(0,numSamples):
        point,history,eIndices = getDraw(dim,eIndices,maxIndex,history)
        #print(len(history))

    
    randUniform = convertToRandomCDF(dim,history,h)
    randStandardNorm = CDFtoNorm(randUniform)

    #wtf(history,'ArraySpace.csv')
    wtf(randUniform,'utemp.csv')
    wtf(randStandardNorm,'ntemp.csv')

    cols = range(0,dim)
    randUniform = np.genfromtxt('utemp.csv',delimiter=',',usecols=cols)
    randStandardNorm = np.genfromtxt('ntemp.csv',delimiter=',',usecols=cols)

    os.remove('utemp.csv')
    os.remove('ntemp.csv')

    return randUniform,randStandardNorm

#####################################################################
    #     Radial Basis Function Network functions
#####################################################################
def get_norm(vec):
	# Returns the 2-norm (Euclidean distance) of vec
	a = 0
	for i in range(0,len(vec)):
		a += vec[i]**2.0
	if a == 0:
		return 0
	else:
		return np.sqrt(a)

def get_phi(r,c,rbf_type):
	if rbf_type == 1:
		if r==0:
			return 0
		else:
			return np.log(c*r)*r*r	
	elif rbf_type == 2:
		return np.exp(-1.0*c*r*r)
	elif rbf_type == 3 or rbf_type == 4:
		phi = np.sqrt(r*r + c*c)
		if rbf_type == 4:
			phi = 1.0/phi
		return phi

def norm_params(vec,test='none'):
	normvec = vec-vec
	#print normvec.shape
	if type(test) != type(vec):
		for j in range(0,normvec.shape[1]):
			maxval = np.nanmax(vec[:,j])
			minval = np.nanmin(vec[:,j])
			normvec[:,j] = (vec[:,j]-minval)/(maxval-minval)
		return normvec
	elif type(test) == type(vec):
		normtest = test-test
		#print normtest.shape
		for j in range(0,normvec.shape[1]):
			maxval = max(vec[:,j])
			minval = min(vec[:,j])
			normvec[:,j] = (vec[:,j]-minval)/(maxval-minval)
			normtest[:,j] = (test[:,j]-minval)/(maxval-minval)
		return normvec,normtest
        
def lda_matrix(x,y,c,rbf_type):
    # Get number of training points and parameters
    n = x.shape[0]
    y = y.T

    #Set up phi storage variable
    A = np.zeros((n,n))

    # Normalize each dimension to range from 0 to 1
    x = norm_params(x)
    #print x

    # Debugging normalizing
    #print x

    # Get phi matrix (radii)
    for i in range(0,n):
        for j in range(0,n):
            r = get_norm(x[j,:]-x[i,:]) 
            A[i,j] = get_phi(r,c,rbf_type)

    lda = np.zeros((y.shape[0],y.shape[1]))
    # Solve for lambda
    for i in range(0,len(y[0,:])):
        try:
            lda[:,i] = np.dot(np.linalg.inv(A),y[:,i])
        except:
            #print "Phi matrix is singular, using pseudoinverse..."
            lda[:,i] = np.dot(np.linalg.pinv(A),y[:,i])
    return lda, A
    
def ypred_rbf(xxmin,xxmax,Nsteps,x_test,x_train,y_train,c,rbf_type):

	dxx = float((xxmax-xxmin)/(Nsteps-1))
	xx = np.zeros([Nsteps,1],dtype=float)
	
	xx = [xxmin+i*dxx for i in range(0,Nsteps)]
	#Normalize each dimension by max expected value
	x_train,x_test = norm_params(x_train,x_test)
	#print x_train, x_test
	lda, A = lda_matrix(x_train,y_train,c,rbf_type)
    
	#print lda
	y_pred = np.zeros((lda.shape[1],1))
	#Get RBF prediction for test point at each x
	for j in range(0,len(lda[0,:])):
		#print len(y[0,:])
		for i in range(0,len(lda[:,0])):
			r = get_norm(x_test[0,:] - x_train[i,:])
			phi = get_phi(r,c,rbf_type)
			y_pred[j,0] += lda[i,j]*phi
			#print r, phi
	return y_pred,lda,A

def progress_bar(count,total,status=''):
    bar_len = 30
    filled_len = int(round(bar_len*count/float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '*' * filled_len+'-'*(bar_len-filled_len)
    sys.stdout.write('[%s] %s%s %s\r' %(bar,percents,'%',status))
    sys.stdout.flush()
    if count == total:
        sys.stdout.write('\n')
        sys.stdout.flush()

def get_data(filename,doe_points):
    data = np.genfromtxt(filename,delimiter=',',skip_header=1)
    xdata = np.array(data[:,0]).reshape(len(data[:,0]),1)
    ydata = np.array(data[:,1:-1])

    doe = np.genfromtxt(doe_points,delimiter=',',skip_header=1)
    sets = len(ydata[0,:])

    return xdata,ydata,len(ydata[0,:]),doe

def split(xdata,ydata,numsets,trainsets,points):

    training_data = ydata[:,0:trainsets]
    target_data = ydata[:,trainsets:-1]
    training_points = points[0:trainsets,:]
    target_points = points[trainsets:-1,:]
    
    return training_data,training_points,target_data,target_points

def get_rmse(test,true):
    diff = []
    for i in range(0,len(test)):
        diff.append((test[i]-true[i])/true[i])

    diff = [x**2.0 for x in diff]
    sumdiff = np.nansum(diff)
    
    return np.sqrt(sumdiff/len(diff))

def epoch(data,doe,trainsets,cmin,cmax,csteps,rbf_type):
    # Grab training data, training points
    xx,yy,sets,points = get_data(data,doe)
    crange = np.linspace(cmin,cmax,csteps)

    training_data,training_points,target_data,target_points = split(xx,yy,sets,trainsets,points)
    num_targets = len(target_points[:,0])
    Nsteps = target_data.shape[0]

    targeterr = np.zeros((len(crange),num_targets),dtype=float)
    avgtargeterr = np.zeros((len(crange)),dtype=float)

    for i in range(0,num_targets):
        x_test = target_points[i,:].reshape(1,target_points.shape[1])
        y_target = target_data[:,i]
        err = []
        for cval in crange:
            ypred,lda,A = ypred_rbf(0.0,1.0,Nsteps,x_test,training_points,training_data,cval,rbf_type)
            rmse = get_rmse(ypred,y_target)
            err.append(rmse)
        targeterr[:,i] = err

    # Compute average RMSE at each cval
    avgtargeterr = []
    for i in range(0,len(crange)):
        avgtargeterr.append(np.nanmean(targeterr[i,:]))

    minerr = np.nanmin(avgtargeterr)
    cminerr = avgtargeterr.index(minerr)

    yminerr,lda,A = ypred_rbf(0.0,1.0,Nsteps,x_test,training_points,training_data,cminerr,rbf_type)

    #plt.plot(xx,y_target,'k-',linewidth=2,label="Target")
    #plt.plot(xx,yminerr,'-',linewidth=2,label="RBFN_%s" %training_data.shape[1])

    return crange,avgtargeterr

def translateToDist(uniform,normal,mean,stdev,dist):
    dim = uniform.shape[1]
    parameters = np.zeros((uniform.shape[0],uniform.shape[1]),dtype=float)
    for i in range(0,dim):
        # translate to normal
        if dist[i] == 0:
            if stdev[i] != 0 and stdev[i] > 0:
                parameters[:,i] = normal[:,i]*stdev[i]+mean[i]
            else:
                parameters[:,i] = parameters[:,i]+mean[i]
        if dist[i] == 1:
            delta = 2.0*stdev[i]
            minVal = mean[i] - stdev[i]
            if stdev[i] != 0 and stdev[i] > 0:
                parameters[:,i] = uniform[:,i]*delta+minVal
            else:
                parameters[:,i] = parameters[:,i]+mean[i]

    return parameters
            

if __name__ == '__main__':

    # Get parameters file
    pfile = 'jwl_compb_params.csv'
    training_data_file = 'training_data.csv'
    training_point_file='training_points.csv'
    samples = 100
    cval = 0.16237
    rbfType = 'g'

    if rbfType == 't' or rbfType == 'tp' or rbfType == 'thinplate' or rbfType == '1':
        rbfType = 1
    elif rbfType == 'g' or rbfType == 'gaussian' or rbfType == '2':
        rbfType = 2
    elif rbfType == 'mq' or rbfType == 'multiquadric' or rbfType == '3':
        rbfType = 3
    elif rbfType == 'imq' or rbfType == 'inversemulti' or rbfType == '4':
        rbfType = 4

    xx, training_data, sets, training_points = get_data(training_data_file,training_point_file)
    
    params = np.genfromtxt(pfile,delimiter=',',skip_header=1,usecols=(1,2,3))
    paramLabels = np.genfromtxt(pfile,delimiter=',',skip_header=1,usecols=(0),dtype=str)

    means = params[:,0]
    stdevs = params[:,1]
    dists = params[:,2]

    dim = len(training_points[0,:])

    # Build standard inputs
    uni,norm = sample(dim,samples)

    # Translate standard inputs to prescribed distributions
    props = translateToDist(uni,norm,means,stdevs,dists)
    fileheader = ''
    for i in range(0,len(paramLabels)):
        if i < len(paramLabels):
            fileheader += str(paramLabels[i]) + ','
        else:
            fileheader += str(paramLabels[i])

    np.savetxt('translated.csv',props,delimiter=',',header=fileheader)

    mProps = deepcopy(means).reshape(1,len(means))
    ypred,lda,A = ypred_rbf(0.0,1.0,len(xx),mProps,training_points,training_data,cval,rbfType)
    
    plt.plot(xx,ypred,'k-',linewidth=2)
    for i in range(0,sets):
        plt.plot(xx,training_data[:,i],'r-',linewidth=1)
    plt.show()
    
